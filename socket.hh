/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include "descriptor.hh"
#include <sys/socket.h>
#include "syscall.hh"
#include <netdb.h>
#include <errno.h>


namespace bsc {

class TSocket {
public:
    enum SOCKET_TYPE {
        TCP_IPv4,
        TCP_IPv6,
        UDP_IPv4,
        UDP_IPv6,
        UNIX_STREAM,
        UNIX_DGRAM,
    };


    static int CreateSocket(int domain, int type, int proto = 0) {
        int result = socket(domain, type | SOCK_CLOEXEC, proto);
        ESyscallError::Validate(result, "socket");
        return result;
    }


    static int CreateSocket(SOCKET_TYPE sock_type, int proto = 0) {
        int domain, type;
        switch (sock_type) {
        case TCP_IPv4:
            domain = PF_INET;
            type = SOCK_STREAM;
            break;
        case TCP_IPv6:
            domain = PF_INET6;
            type = SOCK_STREAM;
            break;
        case UDP_IPv4:
            domain = PF_INET;
            type = SOCK_DGRAM;
            break;
        case UDP_IPv6:
            domain = PF_INET6;
            type = SOCK_DGRAM;
            break;
        case UNIX_STREAM:
            domain = PF_UNIX;
            type = SOCK_STREAM;
            break;
        case UNIX_DGRAM:
            domain = PF_UNIX;
            type = SOCK_DGRAM;
            break;
        default:
            throw EUStrMessage("invalid socket type");
        }

        return CreateSocket(domain, type, proto);
    }


    static ui16 get_local_port_ipv6(int sock) {
        sockaddr_in6 addr;
        socklen_t addr_len = sizeof(addr);

        int res = ::getsockname(sock, (sockaddr*)&addr, &addr_len);
        ESyscallError::Validate(res, "getsockname");

        if (addr.sin6_family != AF_INET6)
            throw EUStrMessage("get_local_port_ipv6: invalid family");

        return ntohs(addr.sin6_port);
    }


    static ui16 get_local_port_ipv4(int sock) {
        sockaddr_in addr;
        socklen_t addr_len = sizeof(addr);

        int res = ::getsockname(sock, (sockaddr*)&addr, &addr_len);
        ESyscallError::Validate(res, "getsockname");

        if (addr.sin_family != AF_INET)
            throw EUStrMessage("get_local_port_ipv4: invalid family");

        return ntohs(addr.sin_port);
    }


    static sa_family_t get_socket_family(int sock) {
        sockaddr addr;
        socklen_t addr_len = sizeof(addr);

        int res = ::getsockname(sock, (sockaddr*)&addr, &addr_len);
        ESyscallError::Validate(res, "getsockname");

        return addr.sa_family;
    }


    static ui16 get_local_port(int sock) {
        sa_family_t family = get_socket_family(sock);
        switch (family) {
        case AF_INET:
            return get_local_port_ipv4(sock);
        case AF_INET6:
            return get_local_port_ipv6(sock);
        default:
            throw EUStrMessage("get_local_port: unsupported family");
        }
    }
};


class ENameError: public ExceptionUStrFromStrView {
public:
    ENameError(int err): ErrorCode_(err) {}

    virtual StrView getStrView() const override {
        const char* explain = gai_strerror(ErrorCode_);
        return StrView(explain);
    }

    static void Validate(int err) {
        switch (err) {
        case 0:
            return;
        case EAI_SYSTEM:
            throw ESyscallError();
        default:
            throw ENameError(err);
        }
    }

protected:
    int ErrorCode_;
};


template <typename TDerived, typename TBase>
class TCreateSocket: public TBase {
public:
    TCreateSocket(TSocket::SOCKET_TYPE type, int proto = 0)
        : TBase(TSocket::CreateSocket(type, proto))
    {}

    TCreateSocket() = default;

    TCreateSocket(int sock) noexcept
        : TBase(sock)
    {}

    TCreateSocket(TDescriptor sock) noexcept
        : TBase(std::move(sock))
    {}


    void connect(const char* hostname, ui16 port) {
        struct addrinfo hints;
        memset(&hints, 0, sizeof(hints));

        if (TBase::get_descriptor() != -1) {
            struct sockaddr saddr;
            socklen_t len = sizeof(saddr);
            int res = getsockname(TBase::get_descriptor(), &saddr, &len);
            ESyscallError::Validate(res, "getsockname");
            hints.ai_family = saddr.sa_family;

            len = sizeof(hints.ai_socktype);
            res = getsockopt(TBase::get_descriptor(), SOL_SOCKET, SO_TYPE,
                    &hints.ai_socktype, &len);
            ESyscallError::Validate(res, "getsockopt");
        }

        struct addrinfo* info;
        int res = getaddrinfo(hostname, nullptr, &hints, &info);
        ENameError::Validate(res);

        switch (info->ai_family) {
        case AF_INET:
            ((sockaddr_in*)(info->ai_addr))->sin_port = htons(port);
            break;
        case AF_INET6:
            ((sockaddr_in6*)(info->ai_addr))->sin6_port = htons(port);
            break;
        }

        if (TBase::get_descriptor() == -1)
            TBase::reset(TSocket::CreateSocket(
                info->ai_family, TDerived::get_socket_type(), 0));

        for (;;) {
            res = ::connect(
                TBase::get_descriptor(), info->ai_addr, info->ai_addrlen);
            if (res == 0) {
                freeaddrinfo(info);
                break;
            }
            if (errno == EINTR)
                continue;
            freeaddrinfo(info);
            throw ESyscallError();
        }
    }


    void connect(UStr hostname, ui16 port) {
        connect(hostname.get_c_str().data(), port);
    }
};


} // namespace bsc
