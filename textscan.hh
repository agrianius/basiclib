/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <algorithm>
#include "types.hh"
#include <iostream>
#include <sstream>
#include <limits>

template<typename TIter, typename TException = const char*>
class TTextScanner {
public:
    TTextScanner(TIter const start, TIter const stop, TException const exception)
        : pivot_(start)
        , end_(stop)
        , exception_(exception)
    {}

    template<typename TResult>
    TResult GetTextToDelim(const char delim) {
        const char del[2] = {delim, '\0'};
        auto res = std::search(pivot_, end_, del, del + 1);
        if (res == end_)
            throw exception_;
        auto resText = TResult(pivot_, res);
        pivot_ = res + 1;
        return resText;
    }

    template<typename TResult>
    TResult GetTextToDelim(const char delim,
                           const ui64 lo_allowed,
                           const ui64 hi_allowed)
    {
        const char del[2] = {delim, '\0'};
        auto res = std::search(pivot_, end_, del, del + 1);
        if (res == end_)
            throw exception_;
        for (auto i = pivot_; i != res; ++i) {
            char sym = *i;
            if (sym > 63) {
                if (not ((1ULL << (sym - 64)) & hi_allowed))
                    throw exception_;
            } else {
                if (not ((1ULL << sym) & lo_allowed))
                    throw exception_;
            }
        }
        auto resText = TResult(pivot_, res);
        pivot_ = res + 1;
        return resText;
    }

    template<typename TResult>
    TResult GetRestText() {
        return TResult(pivot_, end_);
    }

    void SkipChar(char sym) {
        if (*pivot_ != sym)
            throw exception_;
        ++pivot_;
    }

    template<typename TCmpIter>
    void SkipText(TCmpIter cmpFirst, TCmpIter const cmpLast) {
        for (;;) {
            if (cmpFirst == cmpLast)
                return;
            if (pivot_ == end_)
                throw exception_;
            if (*cmpFirst != *pivot_)
                throw exception_;
            ++cmpFirst;
            ++pivot_;
        }
    }

    template<typename TInt>
    TInt GetNumber() {
        if (pivot_ == end_)
            throw exception_;
        TInt value = 0;
        TInt sign = 1;
        if (std::numeric_limits<TInt>::is_signed) {
            if (*pivot_ == '-') {
                sign = -1;
                ++pivot_;
            }
        }
        {
            char cc = *pivot_;
            if (cc < '0' || cc > '9')
                throw exception_;
            value = cc - '0';
        }
        for (;;) {
            ++pivot_;
            if (pivot_ == end_)
                break;
            char cc = *pivot_;
            if (cc < '0' || cc > '9')
                break;
            value = value * 10 + cc;
        }
        return sign * value;
    }

protected:
    TIter pivot_;
    TIter const end_;
    TException const exception_;
};

constexpr ui64 BITMASK_LO_CTL = 0xffffffffULL;
constexpr ui64 BITMASK_HI_DEL = 1ULL << 63;
constexpr ui64 BITMASK_LO_SEPARATORS =
    (1ULL << '(') | (1ULL << ')') | (1ULL << '<') | (1ULL << '>') |
    (1ULL << ' ') | (1ULL << '=') | (1ULL << '"') | (1ULL << ',') |
    (1ULL << ';') | (1ULL << ':') | (1ULL << '?') | (1ULL << '/') |
    (1ULL << '\t');
constexpr ui64 BITMASK_HI_SEPARATORS =
    (1ULL << ('@' - 64)) | (1ULL << ('[' - 64)) | (1ULL << ('\\' - 64)) |
    (1ULL << (']' - 64)) | (1ULL << ('{' - 64)) | (1ULL << ('}' - 64));

constexpr ui64 BITMASK_HI_UPALPHA = 0x7FFFFFEULL;
constexpr ui64 BITMASK_HI_LOALPHA = 0x7FFFFFEULL << 32;
constexpr ui64 BITMASK_HI_ALPHA = BITMASK_HI_UPALPHA | BITMASK_HI_LOALPHA;
constexpr ui64 BITMASK_LO_DIGIT = 0x3FFULL << 48;
constexpr ui64 BITMASK_LO_ALPHANUM = BITMASK_LO_DIGIT;
constexpr ui64 BITMASK_HI_ALPHANUM = BITMASK_HI_ALPHA;

template<typename TIter, typename TInt = ui32>
std::pair<TInt, TIter> GetNumberHex(TIter it, TIter const end) {
    if (it == end)
        throw "Empty range";
    TInt value = 0;
    for (;;) {
        char cc = *it;
        if (cc >= '0' && cc <= '9') {
            value = cc - '0';
            break;
        }
        if (cc >= 'a' && cc <= 'f') {
            value = cc - 'a' + 10;
            break;
        }
        if (cc >= 'A' && cc <= 'F') {
            value = cc - 'A' + 10;
            break;
        }
        throw "Not a number";
    }
    for (;;) {
        ++it;
        if (it == end)
            break;
        char cc = *it;
        if (cc >= '0' && cc <= '9') {
            value = value * 16 + cc - '0';
            continue;
        }
        if (cc >= 'a' && cc <= 'f') {
            value = value * 16 + cc - 'a' + 10; 
            continue;
        }
        if (cc >= 'A' && cc <= 'F') {
            value = value * 16 + cc - 'A' + 10;
            continue;
        }
        break;
    }
    return std::pair<TInt, TIter>(value, it);
}

template<typename TInt>
std::string ToHexString(TInt value) {
    std::ostringstream out;
    out << std::hex << value;
    out.flush();
    return out.str();
}
