/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <unistd.h>

namespace bsc {

class TDescriptor {
public:
    TDescriptor(int fd = -1): FD_(fd) {}

    TDescriptor(const TDescriptor&) = delete;

    TDescriptor(TDescriptor&& move): FD_(move.FD_) {
        move.FD_ = -1;
    }

    TDescriptor& operator=(const TDescriptor&) = delete;

    TDescriptor& operator=(TDescriptor&& move) {
        Close_();
        FD_ = move.FD_;
        move.FD_ = -1;
        return *this;
    }

    void reset(int newfd) {
        if (FD_ == newfd)
            return;
        Close_();
        FD_ = newfd;
    }

    bool empty() const {
        return FD_ == -1;
    }

    void close() {
        Close_();
        FD_ = -1;
    }

    ~TDescriptor() {
        Close_();
    }

    int get_descriptor() const noexcept {
        return FD_;
    }

protected:
    int FD_;

    void Close_() {
        if (FD_ != -1)
            // FIXME: make portable close, see man close in case of error
            ::close(FD_);
    }
};



} // namespace bsc
