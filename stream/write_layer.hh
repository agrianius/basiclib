/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include "../exception.hh"
#include "../syscall.hh"
#include <vector>
#include <exception>
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>


namespace bsc {

constexpr size_t DEFAULT_FLUSH_SIZE = 4096;


class EWriteError: public EUStrMessage {
public:
    EWriteError(): EUStrMessage(message) {}

protected:
    static const char* const message;
};


template <typename TBase>
class TWriteHelpers {
public:
    void sure_write(const void* buf, size_t amount) {
        while (amount > 0) {
            size_t written = ((TBase*)this)->write(buf, amount);
            if (written == 0)
                throw EWriteError();
            buf = (const char*)buf + written;
            amount -= written;
        }
    }
};


template <typename TBase>
class TUnbufWriteLB
    : public TBase
    , public TWriteHelpers<TUnbufWriteLB<TBase>>
{
public:
    template <typename...TParams>
    TUnbufWriteLB(TParams&&...params)
        : TBase(std::forward<TParams>(params)...)
    {}

    using TBase::get_descriptor;

    size_t write(void* buf, size_t amount) {
        for (;;) {
            ssize_t result = ::write(get_descriptor(), buf, amount);
            if (result == -1) {
                if (errno != EINTR)
                    throw ESyscallError();
            } else {
                return (size_t) result;
            }
        }
    }

    void sync() const {
        if (::fsync(get_descriptor()) == -1)
            throw ESyscallError();
    }
};


template <typename TBase>
class TUnbufSendLB
    : public TBase
    , public TWriteHelpers<TUnbufWriteLB<TBase>>
{
public:
    template <typename...TParams>
    TUnbufSendLB(TParams&&...params)
        : TBase(std::forward<TParams>(params)...)
    {}

    using TBase::get_descriptor;

    size_t write(void* buf, size_t amount) {
        for (;;) {
            ssize_t result =
                ::send(get_descriptor(), buf, amount, MSG_NOSIGNAL);
            if (result == -1) {
                if (errno != EINTR)
                    throw ESyscallError("send");
            } else {
                return (size_t) result;
            }
        }
    }
};


template <typename TBase>
class TBufWriteLB
    : public TBase
    , public TWriteHelpers<TBufWriteLB<TBase>>
{
public:
    template <typename...TParams>
    TBufWriteLB(TParams&&...params)
        : TBase(std::forward<TParams>(params)...)
    {}

    ~TBufWriteLB() noexcept(false) {
        if (get_descriptor() == -1)
            return;
        if (std::uncaught_exception())
            return;
        flush();
    }

    using TBase::get_descriptor;
    using TWriteHelpers<TBufWriteLB<TBase>>::sure_write;

    void write(const void* buf, size_t amount) {
        Buf_.insert(Buf_.end(), (const char*) buf, (const char*) buf + amount);

        if (Buf_.size() >= DEFAULT_FLUSH_SIZE) {
            size_t written = ((TBase*)this)->write(Buf_.data(), Buf_.size());
            Buf_.erase(Buf_.begin(), Buf_.begin() + written);
        }
    }

    void flush() {
        while (Buf_.size() > 0) {
            size_t written = ((TBase*)this)->write(Buf_.data(), Buf_.size());
            Buf_.erase(Buf_.begin(), Buf_.begin() + written);
        }
    }

    size_t get_buf_size() const {
        return Buf_.size();
    }

protected:
    std::vector<char> Buf_;
};


} // namespace bsc
