/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include "../exception.hh"
#include "../syscall.hh"
#include "../str/refstr.hh"
#include "../compiler.hh"

#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <vector>


namespace bsc {

constexpr size_t DEFAULT_BUFFER_SIZE = 1024;


class EFileTooShort: public EUStrMessage {
public:
    EFileTooShort(): EUStrMessage(message) {}

private:
    static const char* const message;
};


template <typename TReader>
class TReadHelpers {
public:
    void sure_read(void* buf, size_t amount) {
        while (amount > 0) {
	    size_t filled = static_cast<TReader*>(this)->read(buf, amount);
            if (filled == 0) {
                // FIXME: store read amount of data
                throw EFileTooShort();
            }
            buf = (char*)buf + filled;
            amount -= filled;
        }
    }

    std::vector<char> read(size_t amount) {
        std::vector<char> buf;
        buf.resize(amount);
        size_t filled = static_cast<TReader*>(this)->read(buf.data(), amount);
        buf.resize(filled);
        return buf;
    }

    std::vector<char> sure_read(size_t amount) {
        std::vector<char> buf;
        buf.resize(amount);
        sure_read(buf.data(), amount);
        return buf;
    }

    std::vector<char> read_all() {
        std::vector<char> buf(4096);
        for (size_t buf_filled = 0;;) {
            for (size_t space_left = buf.size() - buf_filled; space_left > 0;) {
                size_t filled =
                    static_cast<TReader*>(this)->read(
                        &*(buf.begin() + buf_filled), space_left);
                if (filled == 0) {
                    buf.resize(buf_filled);
                    return buf;
                }
                buf_filled += filled;
                space_left -= filled;
            }
            buf.resize(buf.size() * 4);
        }
    }

    void sure_skip(size_t amount) {
        while (amount) {
            size_t skipped = static_cast<TReader*>(this)->skip(amount);
            if (skipped == 0)
                throw EFileTooShort();
        }
    }


protected:
    TReadHelpers() = default; // prevent wrong usage
};


template <typename TBase, size_t BUFFER_SIZE = DEFAULT_BUFFER_SIZE>
class TReadlineHelper {
public:
    // FIXME: optimize it!
    template <typename TResult = TRefStr>
    TResult readline() {
        TResult result;
        std::vector<char> chunk;
        void* ptr; // it's safe not to initialize it here
        for (;;) {
            chunk = static_cast<TBase*>(this)->read(BUFFER_SIZE);
            if (chunk.size() == 0)
                return result;
            ptr = memchr(chunk.data(), '\n', chunk.size());
            if (ptr != nullptr)
                break;
            result.append(chunk.begin(), chunk.end());
        }
        ptr = (void*)((char*)ptr + 1);
        result.append((const char*) chunk.data(), (const char*) ptr);
        static_cast<TBase*>(this)->unread((char*) ptr, &*chunk.end() - (char*) ptr);
        return result;
    }

protected:
    TReadlineHelper() = default; // prevent wrong usage
};


template <typename TBaseFile>
class TUnbufReadLB
    : public TBaseFile
    , public TReadHelpers<TUnbufReadLB<TBaseFile>>
{
public:
    template <typename ...TParams>
    TUnbufReadLB(TParams&&...params)
        : TBaseFile(std::forward<TParams>(params)...)
    {}

    size_t read(void* buf, size_t amount) {
        for (;;) {
            ssize_t result = ::read(TBaseFile::get_descriptor(), buf, amount);
            if (result == -1) {
                if (errno != EINTR)
                    throw ESyscallError();
            } else {
                return (size_t) result;
            }
        }
    }

    void skip(size_t amount) {
        if (lseek64(TBaseFile::get_descriptor(), amount, SEEK_CUR) != -1)
            return;
        if (errno != ESPIPE)
            throw ESyscallError();
        char buf[4096];
        while (amount > 4096) {
            size_t res = read(buf, 4096);
            if (res == 0)
                return;
            amount -= res;
        }
        while (amount > 0) {
            size_t res = read(buf, amount);
            if (res == 0)
                return;
            amount -= res;
        }
    }
};


template <typename TBaseRead,
          size_t BUFFER_SIZE = DEFAULT_BUFFER_SIZE>
class TBufReadLB
    : public TBaseRead
    , public TReadHelpers<TBufReadLB<TBaseRead, BUFFER_SIZE>>
    , public TReadlineHelper<TBufReadLB<TBaseRead, BUFFER_SIZE>>
{
public:
    template <typename ...TParams>
    TBufReadLB(TParams&&...params)
        : TBaseRead(std::forward<TParams>(params)...)
    {}

    using TReadHelpers<TBufReadLB<TBaseRead, BUFFER_SIZE>>::read;
    using TReadHelpers<TBufReadLB<TBaseRead, BUFFER_SIZE>>::sure_read;
    using TReadlineHelper<TBufReadLB<TBaseRead, BUFFER_SIZE>>::readline;
    using TReadHelpers<TBufReadLB<TBaseRead, BUFFER_SIZE>>::read_all;

    size_t read(void* buf, size_t amount) {
        // FIXME: reduce number of memcpy
        if (Buf_.size() == 0) {
            FillBuffer();
            if (Buf_.size() == 0)
                return 0;
        }
        size_t buf_amount = std::min(Buf_.size(), amount);
        ::memcpy(buf, Buf_.data(), buf_amount);
        Buf_.erase(Buf_.begin(), Buf_.begin() + buf_amount);
        return buf_amount;
    }

    void unread(void* buf, size_t amount) {
        Buf_.insert(Buf_.begin(), (const char*) buf,
                (const char*) buf + amount);
    }

    size_t skip(size_t amount = 1) {
        size_t erase = std::min(amount, Buf_.size());
        if (erase > 0) {
            Buf_.erase(Buf_.begin(), Buf_.begin() + erase);
            amount -= erase;
        }
        if (amount == 0)
            return erase;
        else
            return TBaseRead::skip(amount) + erase;
    }

    size_t get_buffered_amount() const {
        return Buf_.size();
    }

private:
    void FillBuffer() {
        Buf_.resize(BUFFER_SIZE);
        size_t filled = TBaseRead::read(Buf_.data(), BUFFER_SIZE);
        Buf_.resize(filled);
    }

    // FIXME: chain buffer?
    std::vector<char> Buf_;
};


} // namespace bsc
