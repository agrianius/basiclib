/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include "types.hh"

#include <time.h>
#include <utility>
#include <string>
#include "exception.hh"
#include <sstream>
#include <limits>

static inline void SleepMiliseconds(ui64 msec) {
    timespec ts = {time_t(msec / 1000), long((msec % 1000) * 1000000)};
    nanosleep(&ts, nullptr);
}

template <typename T>
void Unused(T&&) {}

class TTimeSpec {
public:
    enum { INIT_NONE, INIT_ZERO, INIT_MONOTONIC };
    TTimeSpec(int init = INIT_NONE) {
        switch (init) {
        default:
            break;
        case INIT_ZERO:
            timestamp_.tv_sec = 0;
            timestamp_.tv_nsec = 0;
            break;
        case INIT_MONOTONIC:
            clock_gettime(CLOCK_MONOTONIC, &timestamp_);
            break;
        }
    }

    void AddNanoSeconds(long int nanosecs) {
        timestamp_.tv_nsec += nanosecs;
        if (timestamp_.tv_nsec >= 1000000000) {
            timestamp_.tv_sec += timestamp_.tv_nsec / 1000000000;
            timestamp_.tv_nsec %= 1000000000;
            return;
        }
        if (timestamp_.tv_nsec < 0) {
            time_t addSecs = timestamp_.tv_nsec / -1000000000 + 1;
            timestamp_.tv_sec -= addSecs;
            timestamp_.tv_nsec += addSecs * 1000000000;
        }
    }

    void AddMiliSeconds(int milisecs) {
        AddNanoSeconds((long int) milisecs * 1000000L);
    }

    long int NanoSecondsLeft() const {
        timespec current;
        clock_gettime(CLOCK_MONOTONIC, &current);
        long int diffSeconds = timestamp_.tv_sec - current.tv_sec;
        long int diffNanoSeconds = timestamp_.tv_nsec - current.tv_nsec;
        return diffSeconds * 1000000000 + diffNanoSeconds;
    }

    int MiliSecondsLeft() const {
        return NanoSecondsLeft() / 1000000;
    }
protected:
    timespec timestamp_;
};

class TTimeoutClock {
public:
    TTimeoutClock(int miliseconds);
protected:

};

template<typename TIter, typename TInt = ui32>
std::pair<TInt, TIter> GetNumber(TIter it, TIter const end) {
    if (it == end)
        throw "Empty range";
    TInt value = 0;
    TInt sign = 1;
    if (std::numeric_limits<TInt>::is_signed) {
        if (*it == '-') {
            sign = -1;
            ++it;
        }
    }
    {
        char cc = *it;
        if (cc < '0' || cc > '9')
            throw "Not a number";
        value = cc - '0';
    }
    for (;;) {
        ++it;
        if (it == end)
            break;
        char cc = *it;
        if (cc < '0' || cc > '9')
            break;
        value = value * 10 + cc - '0';
    }
    return std::pair<TInt, TIter>(sign * value, it);
}

template<typename TIter, typename TInt = ui32>
TInt GetNumberStrict(TIter begin, TIter const end) {
    auto pair = GetNumber<TIter, TInt>(begin, end);
    if (pair.second != end)
        throw "String is not a number";
    return pair.first;
}

template<typename TInt = ui32>
TInt GetNumberFromStdString(std::string::const_iterator begin,
                            const std::string::const_iterator end) {
    return GetNumberStrict<std::string::const_iterator, TInt>(begin, end);
}

template<typename TInt = ui32>
TInt GetNumberFromStdString(const std::string& str) {
    return GetNumberStrict<std::string::const_iterator, TInt>(
         str.begin(), str.end());
}

template<typename TIter>
std::pair<TIter, TIter> Trim(TIter begin, TIter end) {
    while (begin != end) {
        if (*begin == ' ' || *begin == '\t' ||
            *begin == '\r' || *begin == '\n') {
            begin++;
        } else break;
    }
    while (begin != end) {
        auto pend = end;
        --pend;
        if (*pend == ' ' || *pend == '\t' ||
            *pend == '\r' || *pend == '\n') {
            end = pend;
        } else break;
    }
    return std::pair<TIter, TIter>(begin, end);
}

template<typename TString>
TString Trim(TString str) {
    auto pair = Trim(str.begin(), str.end());
    return TString(pair.first, pair.second);
}

template<typename TResult, typename TString>
TResult TrimAndGetNumber(const TString& str) {
    auto pair = Trim(str.begin(), str.end());
    return GetNumberStrict<decltype(str.begin()), TResult>(
        pair.first, pair.second);
}

void UpperString(char* start, char* stop);
void UpperString(std::string& str);
void LowerString(char* start, char* stop);
void LowerString(std::string& str);

template<typename TInt>
std::string ToString(TInt value) {
    std::ostringstream out;
    out << value;
    out.flush();
    return out.str();
}

