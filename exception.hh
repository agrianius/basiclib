/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include "types.hh"
#include "str/string.hh"
#include <utility>
#include <type_traits>

namespace bsc {

namespace detail {
    class ExceptionBase;
}


class detail::ExceptionBase {
public:
    virtual ~ExceptionBase() = default;
    virtual StrView getStrView() const;
    virtual UStr getUStr() const;
};


template <typename TFirst, typename TSecond>
class ExceptionGlue
    : public TFirst
    , public TSecond
{
public:
    template <typename TFirstParam, typename TSecondParam>
    ExceptionGlue(TFirstParam&& first, TSecondParam&& second)
        : TFirst(std::forward<TFirstParam>(first))
        , TSecond(std::forward<TSecondParam>(second))
    {}
};


class Exception: virtual public detail::ExceptionBase {
};



class ExceptionStrViewFromUStr: public Exception {
public:
    virtual StrView getStrView() const override;
};


class ExceptionUStrFromStrView: public Exception {
public:
    virtual UStr getUStr() const override;
};


template <typename TFirst, typename TSecond>
std::enable_if_t<std::is_base_of<Exception, TFirst>::value,
				 ExceptionGlue<TFirst, TSecond>>
operator<<(TFirst&& first, TSecond&& second) {
	return ExceptionGlue<TFirst, TSecond>(
		std::forward<TFirst>(first), std::forward<TSecond>(second));
}


class EUStrMessage: public Exception {
public:
    explicit EUStrMessage(const char* msg)
        : Buf_(msg)
    {}

    explicit EUStrMessage(UStr buf)
	: Buf_(std::move(buf))
    {}

    virtual StrView getStrView() const override;
    virtual UStr getUStr() const override;
    
protected:
    const UStr Buf_;
};

}
