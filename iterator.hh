/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include "types.hh"
#include <type_traits>

namespace bsc {

template <typename TIter>
ptrdiff_t Distance(TIter from, TIter to) noexcept {
	return to - from;
}


template <typename TIter>
struct ContinuousPODIter {
	constexpr static bool value = false;
};

template <typename TIter>
struct ContinuousPODIter<TIter*> {
	constexpr static bool value = std::is_trivially_copyable<TIter>::value;
};

template <typename TIterDst, typename TIterSrc>
void Copy(TIterDst dst, TIterSrc begin, TIterSrc end) {
	if (ContinuousPODIter<TIterDst>::value &&
			ContinuousPODIter<TIterSrc>::value &&
			sizeof(*&*dst) == sizeof(*&*begin)) {
		memcpy(&*dst, &*begin, Distance(begin, end) * sizeof(*&*dst));
	} else {
		for (; begin != end; ++dst, ++begin)
			*dst = *begin;
	}
}

template <typename TIterDst, typename TIterSrc>
void Move(TIterDst dst, TIterSrc begin, TIterSrc end) {
	if (ContinuousPODIter<TIterDst>::value &&
			ContinuousPODIter<TIterSrc>::value &&
			sizeof(*&*dst) == sizeof(*&*begin)) {
		memcpy(&*dst, &*begin, Distance(begin, end) * sizeof(*&*dst));
	} else {
		for (; begin != end; ++dst, ++begin)
			*dst = std::move(*begin);
	}
}

template <typename TContainer>
struct TContainerIter;

template <typename TContainer>
using Iter = typename TContainerIter<TContainer>::Type;


template <typename TContainer>
struct TContainerConstIter;

template <typename TContainer>
using ConstIter = typename TContainerConstIter<TContainer>::Type;


template <typename TContainer>
Iter<TContainer> Begin(TContainer&& obj) {
	return obj.Begin();
}

template <typename TContainer>
Iter<TContainer> End(TContainer&& obj) {
	return obj.End();
}


template <typename TContainer>
ConstIter<TContainer> CBegin(TContainer&& obj) {
	return obj.CBegin();
}

template <typename TContainer>
ConstIter<TContainer> CEnd(TContainer&& obj) {
	return obj.CEnd();
}

} // namespace bsc
