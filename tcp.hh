/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include "descriptor.hh"
#include "socket.hh"
#include "stream/read_layer.hh"
#include "stream/write_layer.hh"

namespace bsc {

class TTcpV4Socket
    : public TCreateSocket<TTcpV4Socket,
          TBufWriteLB<TUnbufSendLB<
              TBufReadLB<TUnbufReadLB<TDescriptor>>>>>
{
public:
    TTcpV4Socket(): TCreateSocket(TSocket::TCP_IPv4) {}

    static int get_socket_type() noexcept {
        return SOCK_STREAM;
    }
};


class TTcpV6Socket
    : public TCreateSocket<TTcpV6Socket,
          TBufWriteLB<TUnbufSendLB<
              TBufReadLB<TUnbufReadLB<TDescriptor>>>>>
{
public:
    TTcpV6Socket(): TCreateSocket(TSocket::TCP_IPv6) {}

    static int get_socket_type() noexcept {
        return SOCK_STREAM;
    }
};


class TTcpSocket
        : public TCreateSocket<TTcpSocket,
              TBufWriteLB<TUnbufSendLB<
                  TBufReadLB<TUnbufReadLB<TDescriptor>>>>>
{
public:
    TTcpSocket() = default;

    explicit TTcpSocket(int sock)
        : TCreateSocket<TTcpSocket,
            TBufWriteLB<TUnbufSendLB<
                TBufReadLB<TUnbufReadLB<TDescriptor>>>>>(sock)
    {}

    explicit TTcpSocket(TDescriptor sock)
        : TCreateSocket<TTcpSocket,
            TBufWriteLB<TUnbufSendLB<
                TBufReadLB<TUnbufReadLB<TDescriptor>>>>>(std::move(sock))
    {}

    static int get_socket_type() noexcept {
        return SOCK_STREAM;
    }
};


constexpr ui32 DEFAULT_QUEUE_LEN = 1024;


class TTcpServer: public TCreateSocket<TTcpServer, TDescriptor> {
public:
    TTcpServer()
        : TCreateSocket(TSocket::TCP_IPv6)
    {}

    explicit TTcpServer(int port, ui32 queue_len = DEFAULT_QUEUE_LEN)
        : TCreateSocket(TSocket::TCP_IPv6)
    {
        listen(port, queue_len);
    }

    void set_reuse_address(int flag = 1) {
        int ret = ::setsockopt(
            get_descriptor(), SOL_SOCKET, SO_REUSEADDR, &flag, sizeof(flag));
        ESyscallError::Validate(ret, "setsockopt: SO_REUSEADDR");
    }

    void bind(int port) {
        struct sockaddr_in6 server_addr;
        server_addr.sin6_family = AF_INET6;
        server_addr.sin6_addr = in6addr_any;
        server_addr.sin6_port = htons(port);

        int ret = ::bind(
            get_descriptor(), (sockaddr*)&server_addr, sizeof(server_addr));
        ESyscallError::Validate(ret, "bind");
    }

    void listen(int port, ui32 queue_len = DEFAULT_QUEUE_LEN) {
        set_reuse_address();
        bind(port);

        int ret = ::listen(get_descriptor(), queue_len);
        ESyscallError::Validate(ret, "listen");
    }

    TTcpSocket accept() {
        int sock = ::accept4(get_descriptor(), nullptr, nullptr, SOCK_CLOEXEC);
        ESyscallError::Validate(sock, "accept4");
        return TTcpSocket(sock);
    }
};

} // namespace bsc
