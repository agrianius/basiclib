/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include "../types.hh"
#include <atomic>
#include <new>
#include <string.h>
#include "string.hh"
#include "../iterator.hh"
#include <algorithm>

namespace bsc {

class TRefCounter {
public:
    TRefCounter() = default;
    TRefCounter(size_t init): Counter_(init) {}

    size_t get_ref_counter() const {
        return Counter_.load(std::memory_order_acquire);
    }

    size_t add_ref() const {
        return ++Counter_;
    }

    size_t rem_ref() const {
        return --Counter_;
    }

    size_t add_ref_nothreads() const {
        return Counter_.fetch_add(1, std::memory_order_relaxed) + 1;
    }

    size_t rem_ref_nothreads() const {
        return Counter_.fetch_sub(1, std::memory_order_relaxed) - 1;
    }

    bool is_alone() const {
        return Counter_.load() == 1;
    }

protected:
    mutable std::atomic<size_t> Counter_;
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"

class TRefBuf final
        : public TRefCounter
        , public TUniRefController
{
public:
    static TRefBuf* AllocNew(std::size_t amount) {
        void* buffer = ::operator new(sizeof(TRefBuf) + amount);
        TRefBuf* self = new (buffer) TRefBuf(amount);
        return self;
    }

    TRefBuf* AllocCopy(size_t reserve = 0) const {
    	if (reserve < Filled_ + 1)
    		reserve = Filled_ + 1;
        void* buffer = ::operator new(sizeof(TRefBuf) + reserve);
        TRefBuf* copy = new (buffer) TRefBuf(*this);
        return copy;
    }

    template <typename TIter>
    static TRefBuf* AllocNew(TIter begin, TIter end) {
    	size_t amount = Distance(begin, end) + 1;
    	void* buffer = ::operator new(sizeof(TRefBuf) + amount);
    	TRefBuf* self = new (buffer) TRefBuf(begin, end);
    	return self;
    }

    static TRefBuf* AllocNew(StrView buf) {
    	return AllocNew(buf.begin(), buf.end());
    }

    char* data() {
        return &Buf[0];
    }

    size_t get_allocated() const {
        return Allocated_;
    }

    size_t get_filled() const {
        return Filled_;
    }

    void set_filled(size_t new_amount) {
        Filled_ = new_amount;
    }

    virtual void uni_add_ref() const override {
        add_ref();
    }

    virtual void uni_rem_ref() const override {
        rem_ref_and_delete();
    }

    virtual bool is_c_str(const char* end) const override {
        // *end is always accessible because refstr is always has null-char
        // at the end of the string
        return *end == '\0';
    }

    void rem_ref_and_delete() const {
        if (rem_ref() == 0)
            delete this;
    }

    /* NOTE: you can try to make special instance of TRefBuf object in
     * thread local storage */
    static const char* const EmptyString;

protected:
    /* Size of the Buf allocated in memory */
    const size_t Allocated_;

    /* Filled_ field contains amount of stored chars,
       tail null char does not included */
    size_t Filled_;

    char Buf[];

    TRefBuf(size_t amount)
        : TRefCounter(1)
        , Allocated_(amount)
        , Filled_(0)
    {};

    /* Copy only Filled_ amount of char,
       Do not copy memory that does not contain payload */
    TRefBuf(const TRefBuf& copy)
        : TRefCounter(1)
        , Allocated_(copy.Allocated_)
        , Filled_(copy.Filled_)
    {
        memcpy(Buf, copy.Buf, Filled_ + 1);
    }

    template <typename TIter>
    TRefBuf(TIter begin, TIter end)
    	: TRefCounter(1)
		, Allocated_(Distance(begin, end) + 1)
		, Filled_(Distance(begin, end))
	{
    	Copy(Buf, begin, end);
    	Buf[Distance(begin, end)] = 0;
    }
};

#pragma GCC diagnostic pop


class TRefStr;

template <size_t SIZE>
class TBufArray;


class TWeakRefStr: public TraitStringIter {
public:
    TWeakRefStr() = default;
    TWeakRefStr(const TWeakRefStr& copy) = default;
    TWeakRefStr(TWeakRefStr&& move) = default;
    TWeakRefStr& operator=(const TWeakRefStr& copy) = default;
    TWeakRefStr& operator=(TWeakRefStr&& move) = default;

    typedef const char* iterator;
    typedef const char* const_iterator;

    iterator begin() const {
        return Buf_ == nullptr ? TRefBuf::EmptyString : Buf_->data();
    }

    iterator end() const {
        return Buf_ == nullptr ? TRefBuf::EmptyString
                : Buf_->data() + Buf_->get_filled();
    }

    iterator cbegin() const {
        return begin();
    }

    iterator cend() const {
        return end();
    }

    bool empty() const noexcept {
        return Buf_ == nullptr || Buf_->get_filled() == 0;
    }

    const char& operator[](const size_t offset) const {
        return begin()[offset];
    }

    const char& front() const {
        return begin()[0];
    }

    const char& back() const {
        return end()[-1];
    }

    UStr get_uni_str() const {
        return UStr(Buf_, begin(), end());
    }

    StrView get_strbuf() const noexcept {
        return StrView(cbegin(), cend());
    }

    StrView get_trimmed() const noexcept {
        auto buf = get_strbuf();
        buf.trim();
        return buf;
    }

    operator StrView() const noexcept {
        return get_strbuf();
    }

    template <typename TCompare>
    bool operator==(TCompare&& rightOp) const {
        return get_strbuf() == (StrView)rightOp;
    }

    template <typename...TParams>
    StrView get_field(TParams&&...params) {
        auto buf = get_strbuf();
        auto result = buf.get_field(std::forward<TParams>(params)...);
        return result;
    }

    template <typename...TParams>
    StrView get_field_reverse(TParams&&...params) {
        auto buf = get_strbuf();
        auto result = buf.get_field_reverse(std::forward<TParams>(params)...);
        return result;
    }

    inline TBufArray<2> operator+(const StrView& op) const noexcept;

    template <typename TOp>
    inline TBufArray<2> operator+(TOp&& op) const;

    bool operator<(const TWeakRefStr& op) const noexcept {
        return get_strbuf() < op.get_strbuf();
    }

    size_t length() const {
        return Buf_ == nullptr ? 0 : Buf_->get_filled();
    }

    size_t size() const {
        return length();
    }

    const char* data() const noexcept {
        return Buf_ == nullptr ? nullptr : Buf_->data();
    }

protected:
    TRefBuf* Buf_ = nullptr;

    TWeakRefStr(TRefBuf* ptr): Buf_(ptr) {}

    friend class TRefStr;
    friend class TRefBufPtr;
};


// String with reference counting
class TRefStr: public TWeakRefStr {
public:
    using value_type = char;

    TRefStr() = default;

    TRefStr(const TRefStr& copy): TWeakRefStr(copy) {
        if (Buf_ != nullptr)
            Buf_->add_ref();
    }

    TRefStr(TRefStr&& move): TWeakRefStr(move) {
        if (Buf_ != nullptr)
            move.Buf_ = nullptr;
    }

    TRefStr& operator=(const TRefStr& copy) {
        if (Buf_ == copy.Buf_)
            return *this;
        if (Buf_ != nullptr)
            Buf_->rem_ref_and_delete();
        *static_cast<TWeakRefStr*>(this) = static_cast<TWeakRefStr>(copy);
        if (Buf_ != nullptr)
            Buf_->add_ref();
        return *this;
    }

    TRefStr& operator=(TRefStr&& move) {
        if (Buf_ != nullptr)
            Buf_->rem_ref_and_delete();
        *static_cast<TWeakRefStr*>(this) = static_cast<TWeakRefStr>(move);
        if (Buf_ != nullptr)
            move.Buf_ = nullptr;
        return *this;
    }

    TRefStr(TWeakRefStr weak): TWeakRefStr(weak) {
        if (Buf_ != nullptr)
            Buf_->add_ref();
    }

    TRefStr(const char* cstr)
        : TWeakRefStr(TRefBuf::AllocNew(strlen(cstr) + 1))
    {
        memcpy(Buf_->data(), cstr, Buf_->get_allocated());
        Buf_->set_filled(Buf_->get_allocated() - 1);
    }

    TRefStr(const char* cstr, size_t amount)
        : TWeakRefStr(TRefBuf::AllocNew(amount + 1))
    {
        memcpy(Buf_->data(), cstr, amount);
        Buf_->data()[amount] = 0;
        Buf_->set_filled(amount);
    }

    TRefStr(const char* begin, const char* end)
        : TRefStr(begin, end - begin)
    {}

    TRefStr(const StrView& buf)
    	: TRefStr(buf.begin(), buf.end())
    {}

    template <typename TIter>
    TRefStr(TIter&& begin, TIter&& end)
        : TWeakRefStr(TRefBuf::AllocNew(Distance(begin, end) + 1))
	{
    	Copy(Buf_->data(), begin, end);
    	Buf_->data()[Distance(begin, end)] = 0;
    	Buf_->set_filled(Distance(begin, end));
    }

    template <size_t SIZE>
    TRefStr(TBufArray<SIZE>&& array);

    TRefStr(size_t repeat, char chr)
        : TWeakRefStr(TRefBuf::AllocNew(repeat + 1))
    {
    	memset(Buf_->data(), chr, repeat);
    	Buf_->data()[repeat] = 0;
    	Buf_->set_filled(repeat);
    }

    ~TRefStr() {
        if (Buf_ != nullptr)
            Buf_->rem_ref_and_delete();
    }

    /* WARNING: This buffer may be referenced by many instances of TRefStr
       and possibly may be used in many threads.
       Modify the buffer if you know what you are doing */
    const char* c_str() const {
        return Buf_ == nullptr ? TRefBuf::EmptyString : Buf_->data();
    }

    typedef char* iterator;
    typedef const char* const_iterator;

    iterator begin() {
        if (Buf_ == nullptr)
            /* In this case begin() == end() and
             * the iterator must not be used for writing */
            return const_cast<iterator>(TRefBuf::EmptyString);
        PrepareForUpdating();
        return Buf_->data();
    }

    iterator end() {
        if (Buf_ == nullptr)
            /* In this case begin() == end() and
             * the iterator must not be used for writing */
            return const_cast<iterator>(TRefBuf::EmptyString);
        PrepareForUpdating();
        return Buf_->data() + Buf_->get_filled();
    }

    const_iterator begin() const {
        return Buf_ == nullptr ? TRefBuf::EmptyString : Buf_->data();
    }

    const_iterator end() const {
        return Buf_ == nullptr ?
            TRefBuf::EmptyString :
            Buf_->data() + Buf_->get_filled();
    }

    const_iterator cbegin() const {
        return begin();
    }

    const_iterator cend() const {
        return end();
    }

    char& operator[](const size_t offset) {
        PrepareForUpdating();
        return begin()[offset];
    }

    const char& operator[](const size_t offset) const {
        return begin()[offset];
    }

    char& front() {
        PrepareForUpdating();
        return begin()[0];
    }

    const char& front() const {
        return begin()[0];
    }

    char& back() {
        PrepareForUpdating();
        return end()[-1];
    }

    const char& back() const {
        return end()[-1];
    }

    void make_exclusive_buffer() {
        if (Buf_ != nullptr)
            PrepareForUpdating();
    }

    size_t get_reference_count() const {
        return Buf_ == nullptr ? 0 : Buf_->get_ref_counter();
    }

    bool empty() const {
        return Buf_ == nullptr || Buf_->get_filled() == 0;
    }

    size_t length() const {
        return Buf_ == nullptr ? 0 : Buf_->get_filled();
    }

    size_t size() const {
    	return length();
    }

    TRefStr& assign(const_iterator from, const_iterator to);

    TRefStr& assign(const char* c_str) {
        return assign(c_str, c_str + strlen(c_str) + 1);
    }

    TRefStr& operator=(const char* c_str) {
        return assign(c_str);
    }

    UStr get_uni_str() const {
        return UStr(Buf_, cbegin(), cend());
    }

    UStr release_to_uni_str() {
        UStr result(Buf_, cbegin(), cend());
        if (Buf_ != nullptr) {
            Buf_->rem_ref_and_delete();
            Buf_ = nullptr;
        }
        return result;
    }

    template <typename TIter>
    TRefStr& append(TIter from, TIter to) {
    	return insert(cend(), from, to);
    }

    template <typename TContainer>
    void append_all(TContainer&& container) {
    	append(container.cbegin(), container.cend());
    }

    template <typename TIter>
    TRefStr& insert(const_iterator pos, TIter from, TIter to) {
    	ptrdiff_t dist = Distance(from, to);
    	if (dist == 0)
    		return *this;

    	ui64 new_filled = length() + dist;

    	/* Lets check if we could insert new data into existing buffer */
        if (Buf_ != nullptr
                && Buf_->is_alone()
				&& Buf_->get_allocated() > new_filled)
    	{
    		memmove(const_cast<iterator>(pos) + dist, pos, end() - pos + 1);
    		Copy(const_cast<iterator>(pos), from, to);
    		Buf_->set_filled(new_filled);
    		return *this;
    	}

    	/* We have to allocate new buffer */
        auto newbuf = TRefBuf::AllocNew(new_filled + 1);

       	if (!empty()) {
    		memcpy(newbuf->data(), Buf_->data(), Distance(cbegin(), pos));
    		memcpy(newbuf->data() + Distance(cbegin(), pos) + dist, pos,
    			Distance(pos, cend()));
       		Copy(newbuf->data() + Distance(cbegin(), pos), from, to);
       	} else {
       		Copy(newbuf->data(), from, to);
       	}

    	newbuf->set_filled(new_filled);
    	newbuf->data()[new_filled] = 0;
        if (Buf_ != nullptr)
            Buf_->rem_ref_and_delete();
        Buf_ = newbuf;
    	return *this;
    }


    TRefStr& append(const char* str, size_t len) {
    	return append(str, str + len);
    }


    StrView get_strbuf() const noexcept {
        return StrView(cbegin(), cend());
    }

    StrView get_trimmed() const noexcept {
    	auto buf = get_strbuf();
    	buf.trim();
    	return buf;
    }

    void trim() {
        if (Buf_ == nullptr)
            return;
        StrView trimmed_view = get_trimmed();
        if (get_strbuf() == trimmed_view)
            return;
        if (!Buf_->is_alone()) {
            auto obj_ptr = TRefBuf::AllocNew(trimmed_view);
            if (Buf_ != nullptr)
                Buf_->rem_ref_and_delete();
            Buf_ = obj_ptr;
            return;
        }
        if (get_strbuf().cbegin() != trimmed_view.begin())
            memmove(Buf_->data(), trimmed_view.begin(), trimmed_view.size());
        Buf_->set_filled(trimmed_view.size());
        Buf_->data()[trimmed_view.size()] = 0;
    }

    template <typename...TParams>
    StrView get_field(TParams&&...params) {
    	auto buf = get_strbuf();
    	auto result = buf.get_field(std::forward<TParams>(params)...);
    	return result;
    }

    template <typename...TParams>
    StrView get_field_reverse(TParams&&...params) {
    	auto buf = get_strbuf();
    	auto result = buf.get_field_reverse(std::forward<TParams>(params)...);
    	return result;
    }

    operator StrView() const noexcept {
    	return get_strbuf();
    }

    template <typename TCompare>
    bool operator==(TCompare&& rightOp) const {
        return get_strbuf() == (StrView)rightOp;
    }

    template <typename TCompare>
    bool operator!=(TCompare&& rightOp) const {
    	return !this->operator==(rightOp);
    }

    operator TWeakRefStr() const noexcept {
        return TWeakRefStr(Buf_);
    }

    void reserve(size_t amount) {
        if (Buf_ != nullptr && Buf_->get_allocated() > amount)
    		return;
        if (Buf_ == nullptr) {
    		Buf_ = TRefBuf::AllocNew(amount + 1);
    		Buf_->data()[0] = 0;
    	} else {
            auto newbuf = Buf_->AllocCopy(amount + 1);
            Buf_->rem_ref_and_delete();
            Buf_ = newbuf;
    	}
    }

    inline TBufArray<2> operator+(const StrView& op) const noexcept;

    template <typename TOp>
    inline TBufArray<2> operator+(TOp&& op) const;

    bool operator<(const TRefStr& op) const noexcept {
    	return get_strbuf() < op.get_strbuf();
    }

    bool operator<(const char* op) const noexcept {
        return get_strbuf() < StrView(op);
    }

    operator UStr() const noexcept {
    	return UStr(Buf_, begin(), end());
    }


    void resize(size_t amount, char fill);

    void push_back(char chr);

    const char* data() const noexcept {
        return Buf_ == nullptr ? nullptr : Buf_->data();
    }

    void clear() noexcept {
        if (Buf_ == nullptr)
            return;
        Buf_->rem_ref_and_delete();
        Buf_ = nullptr;
    }

protected:
    TRefStr(TRefBuf* buf): TWeakRefStr(buf) {}

    /* WARNING: Check Buf_ == nullptr before calling this method */
    void PrepareForUpdating() {
        if (Buf_->is_alone())
            return;
        auto newbuf = Buf_->AllocCopy();
        Buf_->rem_ref_and_delete();
        Buf_ = newbuf;
    }
};


bool WEAK operator<(const char* op1, const TRefStr& op2) {
    return StrView(op1) < op2.get_strbuf();
}


template <size_t SIZE>
class TBufArray {
public:
	template <size_t LSIZE = SIZE>
    TBufArray(StrView add,
              typename std::enable_if<LSIZE == 1>::type* = 0) noexcept
		: buffers{add}
	{}

	template <size_t LSIZE = SIZE>
    TBufArray(StrView op1,
              StrView op2,
              typename std::enable_if<LSIZE == 2>::type* = 0) noexcept
		: buffers{op1, op2}
	{}

    TBufArray(const TBufArray<SIZE-1>& lesser, StrView add) noexcept {
		Copy(begin(), lesser.begin(), lesser.end());
		buffers[SIZE - 1] = add;
	}

    StrView& operator[](size_t num) noexcept {
		return buffers[num];
	}

    const StrView& operator[](size_t num) const noexcept {
		return buffers[num];
	}

    typedef StrView* iterator;
    typedef const StrView* const_iterator;

	iterator begin() noexcept {
		return &buffers[0];
	}

	iterator end() noexcept {
		return &buffers[SIZE];
	}

	const_iterator cbegin() const noexcept {
		return &buffers[0];
	}

	const_iterator cend() const noexcept {
		return &buffers[SIZE];
	}

	const_iterator begin() const noexcept {
		return &buffers[0];
	}

	const_iterator end() const noexcept {
		return &buffers[SIZE];
	}

	constexpr static size_t size() noexcept {
		return SIZE;
	}

    TBufArray<SIZE+1> operator+(const StrView& op) const noexcept {
		return TBufArray<SIZE+1>(*this, op);
	}

	template <typename TOp>
	TBufArray<SIZE+1> operator+(TOp&& op) const {
        return TBufArray<SIZE+1>(*this, (StrView)op);
	}

	template <typename TContainer>
	void concat_all(TContainer& container) const {
		size_t total_size = 0;
		for (const auto& item: buffers)
			total_size += item.size();
		container.reserve(container.size() + total_size);
		for (const auto& item: buffers)
			container.insert(container.end(), item.begin(), item.end());
	}

	operator TWeakRefStr() const {
		TRefStr result;
		concat_all<TRefStr>(result);
		holder = std::move(result);
		return holder;
	}

	operator TRefStr() const {
		TRefStr result;
		concat_all<TRefStr>(result);
		return result;
	}

	operator UStr() const {
		TRefStr result;
		concat_all<TRefStr>(result);
		return result.release_to_uni_str();
	}

protected:
	mutable TRefStr holder;
    StrView buffers[SIZE];
};


template <size_t SIZE>
TRefStr::TRefStr(TBufArray<SIZE>&& array) {
	array.concat_all(*this);
}



static_assert(ContinuousPODIter<TBufArray<1>::iterator>::value,
		"TBufArray should be ContinuousPOD");
static_assert(ContinuousPODIter<TBufArray<2>::iterator>::value,
		"TBufArray should be ContinuousPOD");
static_assert(ContinuousPODIter<TBufArray<100>::iterator>::value,
		"TBufArray should be ContinuousPOD");


} // namespace bsc


bsc::TBufArray<2> inline
operator+(const bsc::StrView& op1,
        const bsc::StrView& op2) noexcept
{
	return bsc::TBufArray<2>(op1, op2);
}

template <typename TOp>
bsc::TBufArray<2> inline
operator+(const bsc::StrView& op1, TOp&& op2) {
    return bsc::TBufArray<2>(op1, (bsc::StrView)op2);
}


inline bsc::TBufArray<2>
operator+(const char* op1, const bsc::StrView& op2) noexcept {
    return bsc::TBufArray<2>(bsc::StrView(op1), op2);
}

bsc::TBufArray<2> inline
operator+(const char* op1, const bsc::TRefStr& str) noexcept {
	return op1 + str.get_strbuf();
}

bsc::TBufArray<2> inline
operator+(const char* op1, const bsc::TWeakRefStr& str) noexcept {
	return op1 + str.get_strbuf();
}



namespace bsc {

inline TBufArray<2>
TWeakRefStr::operator+(const StrView& op) const noexcept {
	return get_strbuf() + op;
}

template <typename TOp>
inline TBufArray<2> TWeakRefStr::operator+(TOp&& op) const {
	return get_strbuf() + op;
}

inline TBufArray<2>
TRefStr::operator+(const StrView& op) const noexcept {
	return get_strbuf() + op;
}

template <typename TOp>
inline TBufArray<2> TRefStr::operator+(TOp&& op) const {
	return get_strbuf() + op;
}

}
