/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "refstr.hh"

namespace bsc {

const char* const TRefBuf::EmptyString = "";

TRefStr& TRefStr::assign(const_iterator from, const_iterator to) {
    while (from != to && to[-1] == '\0')
        --to;

    size_t len = to - from;

    if (Buf_ == nullptr
        || !Buf_->is_alone()
        || Buf_->get_allocated() < len + 1)
    {
        auto newbuf = TRefBuf::AllocNew(len + 1);
        if (Buf_ != nullptr)
            Buf_->rem_ref_and_delete();
        Buf_ = newbuf;
    }

    memcpy(Buf_->data(), from, len);
    Buf_->data()[len] = 0;
    Buf_->set_filled(len);
    return *this;
}


void TRefStr::resize(size_t amount, char fill) {
	if (amount == size())
		return;

    if (Buf_ != nullptr) {
        if (!Buf_->is_alone()) {
            if (amount == 0) {
                Buf_->rem_ref_and_delete();
                Buf_ = nullptr;
                return;
            }

            auto newbuf = TRefBuf::AllocNew(amount + 1);
            if (amount < size()) {
                memcpy(newbuf->data(), Buf_->data(), amount);
            } else {
                memcpy(newbuf->data(), Buf_->data(), size());
                memset(newbuf->data() + size(), fill, amount - size());
            }
            newbuf->set_filled(amount);
            newbuf->data()[amount] = 0;
            Buf_->rem_ref_and_delete();
            Buf_ = newbuf;
            return;
        }

        if (amount < Buf_->get_filled()) {
            Buf_->set_filled(amount);
            Buf_->data()[amount] = 0;
            return;
        }

        if (amount < Buf_->get_allocated()) {
            memset(end(), fill, amount - size());
            Buf_->set_filled(amount);
            Buf_->data()[amount] = 0;
            return;
        }
	}

    auto newbuf = TRefBuf::AllocNew(amount + 1);
	if (size() != 0)
		memcpy(newbuf->data(), Buf_->data(), size());
	memset(newbuf->data() + size(), fill, amount - size());
	newbuf->set_filled(amount);
	newbuf->data()[amount] = 0;
    if (Buf_ != nullptr)
        Buf_->rem_ref_and_delete();
    Buf_ = newbuf;
}


void TRefStr::push_back(char chr) {
    if (Buf_ != nullptr
			&& Buf_->is_alone()
			&& size() != Buf_->get_allocated() - 1)
	{
		*end() = chr;
		Buf_->set_filled(size() + 1);
		Buf_->data()[size()] = 0;
		return;
	}

	size_t newallocated = std::max<size_t>(size() * 3, 64);
    auto newbuf = TRefBuf::AllocNew(newallocated);
	if (size() != 0)
		memcpy(newbuf->data(), Buf_->data(), size());
	newbuf->data()[size()] = chr;
	newbuf->set_filled(size() + 1);
	newbuf->data()[size() + 1] = 0;
    if (Buf_ != nullptr)
        Buf_->rem_ref_and_delete();
    Buf_ = newbuf;
}


} // namespace bsc
