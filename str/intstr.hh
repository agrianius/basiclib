/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <limits>
#include <type_traits>

namespace bsc {


template <typename TInt, unsigned int ByteSize = sizeof(TInt)>
struct TStringSizeLimit {
    // 309 decimal signs is needed to represent 2^1024
    constexpr static unsigned int value = 309 * ByteSize / (1024 / 8) + 2;
};


template <typename TResult, typename TInt>
TResult ToStr(
        TInt param,
        typename std::enable_if<
                std::numeric_limits<TInt>::is_integer, TInt>::type* = nullptr)
{
    if (param == 0) {
        TResult str;
        str.push_back('0');
        return str;
    }

    char buf[TStringSizeLimit<TInt>::value];
    char* begin = &buf[TStringSizeLimit<TInt>::value];
    const bool negative = param < 0;
    if (param < 0)
        param = -param;

    for (;;) {
        TInt remainder = param % 10;
        TInt delim = param / 10;
        char digits[] = "0123456789";
        *(--begin) = digits[remainder];
        if (delim == 0)
            break;
        param = delim;
    }

    if (negative)
        *(--begin) = '-';

    return TResult(begin, &buf[TStringSizeLimit<TInt>::value]);
}


} // namespace bsc
