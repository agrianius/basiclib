/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <string.h>
#include <utility>
#include <alloca.h>
#include <string.h>
#include "../compiler.hh"
#include "../types.hh"
#include "../iterator.hh"

namespace bsc {


constexpr ui64 BITMASK_LO_SPACE_CHAR =
	(1ULL << ' ') | (1ULL << '\r') | (1ULL << '\t') | (1ULL << '\n') |
	(1ULL << '\f') | (1ULL << '\v');
constexpr ui64 BITMASK_HI_SPACE_CHAR = 0;

constexpr bool IsSpaceChar(i8 chr) noexcept {
	ui8 uchr = (ui8)chr;
	return (1ULL << uchr) &
		(uchr < 128 ? BITMASK_LO_SPACE_CHAR : BITMASK_HI_SPACE_CHAR);
}


struct TraitStringIter {
};


class StrView: public TraitStringIter {
public:
	using TIter = const char*;
	using iterator = TIter;
	using const_iterator = TIter;

    StrView(): Begin_(nullptr), End_(nullptr) {}

    StrView(TIter begin, TIter end)
        : Begin_(begin), End_(end) {}

    StrView(TIter begin, size_t size)
        : Begin_(begin), End_(begin + size) {}

    explicit StrView(TIter c_str)
        : Begin_(c_str), End_(c_str + ::strlen(c_str))
    {}

    template <typename TAnotherIter>
    explicit StrView(TAnotherIter begin, TAnotherIter end)
		: Begin_(&*begin), End_(&*end) {}

    TIter begin() const { return Begin_; }
    TIter cbegin() const { return Begin_; }
    TIter end() const { return End_; }
    TIter cend() const { return End_; }
    size_t size() const { return End_ - Begin_; }
    size_t len() const { return End_ - Begin_; }
    bool empty() const { return Begin_ == End_; }

    void trim() noexcept;

    TIter search(char chr, TIter start_from) const noexcept {
    	void* found =
    		memchr(const_cast<char*>(start_from), chr, End_ - start_from);
    	if (found == nullptr)
    		return end();
    	return (TIter) found;
    }

    TIter search_reverse(char chr, TIter start_from) const noexcept {
    	void* found =
    		memrchr(const_cast<char*>(Begin_), chr, start_from - Begin_);
    	if (found == nullptr)
    		return begin() - 1;
    	return (TIter) found;
    }

    TIter search(char chr) const noexcept {
    	return search(chr, begin());
    }

    template <template <typename...> class TContainer>
    TContainer<StrView>
    split_all(char delim, bool keep_empty = false) const {
        TContainer<StrView> result;
    	for (TIter i = begin(); i != end();) {
    		TIter next = search(delim, i);
    		if (keep_empty || next != i)
    			result.emplace_back(i, next);
    		if (next == end())
    			break;
    		i = next + 1;
    	}
    	return result;
    }

    StrView
	pop_field(
			char delim,
			bool pop_empty = false,
			size_t skip_count = 0) noexcept;

    StrView pop_field(char delim, size_t skip_count) noexcept {
    	return pop_field(delim, false, skip_count);
    }

    StrView
	get_field(
			char delim,
			bool pop_empty = false,
			size_t skip_count = 0) noexcept;

    StrView
	get_field(
			char delim,
			i64 skip_count) noexcept {
    	return get_field(delim, false, skip_count);
    }

    StrView
	get_field(
			char delim,
			i32 skip_count) noexcept {
    	return get_field(delim, false, skip_count);
    }

    StrView get_field(char delim, size_t skip_count) noexcept {
    	return get_field(delim, false, skip_count);
    }

    StrView
	get_field_reverse(
			char delim,
			bool pop_empty = false,
			size_t skip_count = 0) noexcept;

    StrView
	get_field_reverse(
			char delim,
			i64 skip_count) noexcept {
    	return get_field(delim, false, skip_count);
    }

    StrView
	get_field_reverse(
			char delim,
			i32 skip_count) noexcept {
    	return get_field_reverse(delim, false, skip_count);
    }

    StrView get_field_reverse(char delim, size_t skip_count) noexcept {
    	return get_field(delim, false, skip_count);
    }

    bool operator==(const StrView& rightOp) const noexcept {
    	if (size() != rightOp.size())
    		return false;
    	return memcmp(begin(), rightOp.begin(), size()) == 0;
    }

    bool operator==(const char* rightOp) const noexcept {
    	return strncmp(begin(), rightOp, size()) == 0 && rightOp[size()] == 0;
    }

    bool operator!=(const StrView& rightOp) const noexcept {
    	return !(*this == rightOp);
    }

    bool operator!=(const char* rightOp) const noexcept {
    	return strncmp(begin(), rightOp, size()) != 0 || rightOp[size()] != 0;
    }

    template <typename TOp>
    bool operator==(const TOp& rightOp) const noexcept {
        return *this == (StrView)rightOp;
    }

    template <typename TOp>
    bool operator!=(const TOp& rightOp) const noexcept {
        return *this != (StrView)rightOp;
    }

    bool operator<(const StrView rightOp) const noexcept {
    	size_t min_size = size() < rightOp.size() ? size() : rightOp.size();
    	int res = memcmp(begin(), rightOp.begin(), min_size);
    	if (res < 0)
    		return true;
    	if (res > 0)
    		return false;
    	return size() < rightOp.size();
    }

    const char& operator[](ptrdiff_t elem) const noexcept {
        return Begin_[elem];
    }

    const char& at(ptrdiff_t elem) const noexcept {
        return Begin_[elem];
    }

    const char& tail_at(ptrdiff_t elem) const noexcept {
        return End_[elem];
    }

    const char& front() const noexcept {
        return at(0);
    }

    const char& back() const noexcept {
        return tail_at(-1);
    }

    const char* data() const noexcept {
        return Begin_;
    }

protected:
    TIter Begin_;
    TIter End_;
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wnon-virtual-dtor"


class TUniRefController {
public:
    virtual void uni_add_ref() const = 0;
    virtual void uni_rem_ref() const = 0;
    virtual bool is_c_str(const char*) const = 0;
};


class UStr;


/* Use this reference controller for static or stack resided strings */
class TVoidController: public TUniRefController {
public:
    virtual void uni_add_ref() const override {}
    virtual void uni_rem_ref() const override {}
    virtual bool is_c_str(const char*) const override { return false; }

    static TVoidController singleton;

    template <typename ... TParams>
    static UStr get_ref(TParams&& ... params);
};


/* Use this reference controller for static NULL terminated strings */
class TCStringController: public TUniRefController {
public:
    virtual void uni_add_ref() const override {}
    virtual void uni_rem_ref() const override {}
    virtual bool is_c_str(const char* end) const override {
        // *end is always accessible because CStringController is used
        // only for c strings or substrings
        return end != nullptr && *end == '\0';
    }

    static inline UStr get_ref(const char* str);

    static TCStringController singleton;
};

#pragma GCC diagnostic pop


class TRefBuf;

class TUniRefControllerPtr {
public:
    TUniRefControllerPtr(
            const TUniRefController* controller = &TVoidController::singleton)
        : Controller_(controller)
    {
        Controller_->uni_add_ref();
    }

    TUniRefControllerPtr(TRefBuf* src);

    ~TUniRefControllerPtr() {
        Controller_->uni_rem_ref();
    }

    TUniRefControllerPtr(const TUniRefControllerPtr& copy)
        : Controller_(copy.Controller_)
    {
        Controller_->uni_add_ref();
    }

    TUniRefControllerPtr(TUniRefControllerPtr&& move)
        : Controller_(move.Controller_)
    {
        move.Controller_ = &TVoidController::singleton;
    }

    TUniRefControllerPtr& operator=(const TUniRefControllerPtr& copy) {
        copy.Controller_->uni_add_ref();
        Controller_->uni_rem_ref();
        Controller_ = copy.Controller_;
        return *this;
    }

    TUniRefControllerPtr& operator =(TUniRefControllerPtr&& move) {
        Controller_->uni_rem_ref();
        Controller_ = move.Controller_;
        move.Controller_ = &TVoidController::singleton;
        return *this;
    }

    const TUniRefController* operator->() const {
        return Controller_;
    }

protected:
    const TUniRefController* Controller_;
};


class UStr: public StrView {
public:
    template <typename ... TParams>
    UStr(TUniRefControllerPtr holder, TParams&&...args)
        : StrView(std::forward<TParams>(args)...)
        , Holder_(std::move(holder))
    {}

    UStr(const char* cstr): UStr(&TCStringController::singleton, cstr) {}

    UStr(): UStr(&TCStringController::singleton, "") {}

    static UStr wrap_c_str(const char* str) {
        return TCStringController::get_ref(str);
    }

    UStr get_c_str() const;

    bool is_c_str() const {
        return Holder_->is_c_str(End_);
    }

protected:
    TUniRefControllerPtr Holder_;
};


class CStr: public UStr {
public:
	CStr(const char* str): UStr(TCStringController::get_ref(str)) {}
};


UStr inline TCStringController::get_ref(const char* str) {
    return UStr(&singleton, str);
}


template <typename ... TParams>
UStr TVoidController::get_ref(TParams&& ... params) {
    return UStr(&singleton, std::forward<TParams>(params)...);
}


inline char* ALWAYS_INLINE create_c_str_on_stack(const StrView str) {
    char* localstr = (char*) alloca(str.size() + 1);
    memcpy(localstr, str.begin(), str.size());
    localstr[str.size()] = 0;
    return localstr;
}


inline bool
operator==(const char* leftOp, const StrView& rightOp) noexcept {
	return rightOp == leftOp;
}

inline bool
operator!=(const char* leftOp, const StrView& rightOp) noexcept {
	return rightOp != leftOp;
}

} // namespace bsc
