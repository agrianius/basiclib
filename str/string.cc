/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "string.hh"
#include "refstr.hh"

namespace bsc {

TVoidController TVoidController::singleton;
TCStringController TCStringController::singleton;

UStr UStr::get_c_str() const {
    if (is_c_str())
        return *this;
    TRefStr str(Begin_, End_);
    return str.release_to_uni_str();
}

TUniRefControllerPtr::TUniRefControllerPtr(TRefBuf* src)
    : Controller_(src)
{
    Controller_->uni_add_ref();
}


void StrView::trim() noexcept {
	for (;; ++Begin_) {
    	if (Begin_ == End_)
    		return;
    	if (!IsSpaceChar(*Begin_))
    		break;
	}
	// Here at least one char is not space
	// thus no need to check Begin_ == End_
	for (; IsSpaceChar(*(End_ - 1)); --End_)
		;
}


StrView
StrView::pop_field(
		char const delim,
		bool const pop_empty,
		size_t skip_count) noexcept
{
	while (!empty()) {
		TIter pivot = search(delim, Begin_);
		if (pop_empty || pivot != Begin_) {
			if (skip_count > 0) {
				if (pivot == End_)
					break;
				Begin_ = pivot + 1;
				--skip_count;
				continue;
			}
			TIter pop_begin = Begin_;
			Begin_ = pivot + 1;
            return StrView(pop_begin, pivot);
		} else {
			Begin_ = pivot + 1;
		}
	}
    return StrView();
}


StrView
StrView::get_field(
		char const delim,
		bool const pop_empty,
		size_t skip_count) noexcept
{
	for (TIter ibegin = Begin_; ibegin != End_;) {
		TIter pivot = search(delim, ibegin);
		if (pop_empty || pivot != ibegin) {
			if (skip_count > 0) {
				if (pivot == End_)
					break;
				ibegin = pivot + 1;
				--skip_count;
				continue;
			}
            return StrView(ibegin, pivot);
		} else {
			ibegin = pivot + 1;
		}
	}
    return StrView();
}


StrView
StrView::get_field_reverse(
		char const delim,
		bool const pop_empty,
		size_t skip_count) noexcept
{
	for (TIter rend = End_; rend != Begin_ - 1;) {
		TIter pivot = search_reverse(delim, rend);
		if (pop_empty || pivot != rend - 1) {
			if (skip_count > 0) {
				if (pivot == Begin_ - 1)
					break;
				--skip_count;
				rend = pivot;
				continue;
			}
            return StrView(pivot + 1, rend);
		} else {
			rend = pivot;
		}
	}
    return StrView();
}


} // namespace bsc
