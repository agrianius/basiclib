/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once

#include "../str/string.hh"
#include "../str/refstr.hh"
#include <fcntl.h>
#include "../descriptor.hh"
#include "../syscall.hh"


namespace bsc {

constexpr char FILESYSTEM_PATH_DELIMETER = '/';


class TFile {
public:
    enum OPEN_MODE {
        READ = O_RDONLY | O_CLOEXEC,
        WRITE = O_WRONLY | O_TRUNC | O_CREAT | O_CLOEXEC,
        WRITE_NOTRUNCATE = O_WRONLY | O_CREAT | O_CLOEXEC,
        READ_WRITE = O_RDWR | O_CREAT | O_CLOEXEC,
        APPEND = O_WRONLY | O_APPEND | O_CREAT | O_CLOEXEC,
		DIR = O_DIRECTORY,
		JUST_WRITE = O_WRONLY,

        DONT_CREATE = ~O_CREAT,
        EXCLUSIVE = O_EXCL,
        DONT_CLOSE_ON_EXEC = ~O_CLOEXEC,
    };

    static void remove(UStr name, bool ignore_if_no_file = false);
    static void remove(const char* name, bool ignore_if_no_file = false);
    static void rename(UStr from, UStr to);
    static void rename(const char* from, const char* to);
    static off_t get_size(UStr name);
    static bool exists(UStr name);

protected:
    template <typename>
    friend class TOpenFile;

    static int open_file(UStr name, OPEN_MODE mode);
};


template <typename TBase>
class TOpenFile: public TBase {
public:
    TOpenFile() = default;

    using TBase::close;
    using TBase::get_descriptor;

    bool is_open() const {
        return !TBase::empty();
    }

    TOpenFile(UStr name, TFile::OPEN_MODE mode)
        : TBase(TFile::open_file(name, mode))
    {}

    void open(UStr name, TFile::OPEN_MODE mode) {
        TBase::reset(TFile::open_file(name, mode));
    }

    void rewind() {
        int res = lseek64(get_descriptor(), 0, SEEK_SET);
        if (res == -1)
            throw ESyscallError();
    }
};


inline TFile::OPEN_MODE
operator|(TFile::OPEN_MODE op1, TFile::OPEN_MODE op2) noexcept {
	return TFile::OPEN_MODE((ui32)op1 | (ui32)op2);
}


TRefStr path_join(StrView op1, StrView op2);

} // namespace bsc
