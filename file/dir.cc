/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "dir.hh"
#include <sys/syscall.h>
#include <dirent.h>
#include "../syscall.hh"

namespace bsc {

TDir::TDir(UStr path)
	: TOpenFile<TDescriptor>(path, TFile::READ | TFile::DIR)
{}

std::vector<TRefStr> TDir::GetList(LIST_TYPE const flags) {
	std::vector<TRefStr> result;
	rewind();

	for (;;) {
		char buf[4096];

        struct linux_dirent64 {
            ino64_t        d_ino;    /* 64-bit inode number */
            off64_t        d_off;    /* 64-bit offset to next structure */
            unsigned short d_reclen; /* Size of this dirent */
            unsigned char  d_type;   /* File type */
            char           d_name[]; /* Filename (null-terminated) */
        };

		const linux_dirent64* dirp = reinterpret_cast<linux_dirent64*>(buf);
		int filled =
			syscall(SYS_getdents64, get_descriptor(), dirp, sizeof(buf));

		if (filled == -1)
			throw ESyscallError();

		if (filled == 0)
			return result;

		while (reinterpret_cast<const char*>(dirp) < &buf[filled]) {
			LIST_TYPE mask;
			switch (dirp->d_type) {
			case DT_REG:
				mask = LIST_FILE;
				break;
			case DT_DIR:
				mask = LIST_DIR;
				break;
			case DT_FIFO:
				mask = LIST_FIFO;
				break;
			case DT_SOCK:
				mask = LIST_SOCKET;
				break;
			case DT_LNK:
				mask = LIST_LINK;
				break;
			case DT_BLK:
				mask = LIST_BLOCKDEV;
				break;
			case DT_CHR:
				mask = LIST_CHARDEV;
				break;
			default:
				mask = LIST_NONE;
			}

			do {
				if (!(mask & flags))
					break;

				if ((LIST_NOSTD_DIRS & flags) && (dirp->d_type == DT_DIR)) {
					if (strcmp(dirp->d_name, ".") == 0)
						break;
					if (strcmp(dirp->d_name, "..") == 0)
						break;
				}

				result.emplace_back((const char*)dirp->d_name);
			} while (0);

			dirp = reinterpret_cast<const linux_dirent64*>(
				reinterpret_cast<const char*>(dirp) + dirp->d_reclen);
		}
	}
}

} // namespace bsc
