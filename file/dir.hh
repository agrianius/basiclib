/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include "file.hh"
#include "../str/string.hh"
#include "../str/refstr.hh"
#include <vector>

namespace bsc {

class TDir: public TOpenFile<TDescriptor> {
public:
	TDir(UStr path);

	enum LIST_TYPE {
		LIST_NONE = 0,
		LIST_FILE = 1,
		LIST_DIR = 2,
		LIST_FIFO = 4,
		LIST_SOCKET = 8,
		LIST_LINK = 16,
		LIST_BLOCKDEV = 32,
		LIST_CHARDEV = 64,
		LIST_ALL = 127,
		LIST_NOSTD_DIRS = 128,
	};

	std::vector<TRefStr> GetList(LIST_TYPE flags);
};


inline TDir::LIST_TYPE
operator|(TDir::LIST_TYPE op1, TDir::LIST_TYPE op2) noexcept {
	return TDir::LIST_TYPE((ui32)op1 | (ui32)op2);
}


} // namespace bsc
