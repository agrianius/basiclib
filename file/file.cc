/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#include "file.hh"
#include <errno.h>
#include "../compiler.hh"
#include "../syscall.hh"
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>

namespace bsc {


int TFile::open_file(const UStr name, TFile::OPEN_MODE mode) {
    const char* fname = name.is_c_str() ? name.begin()
        : create_c_str_on_stack(name);

    for (;;) {
        int fd = ::open(fname, mode, ~0);
        if (fd == -1) {
            if (errno == EINTR)
                continue;
            throw ESyscallError();
        }
        return fd;
    }
}



void TFile::remove(const char* name, bool ignore_if_no_file) {
    if (::unlink(name) != -1)
        return;
    if (errno == ENOENT && ignore_if_no_file)
        return;
    throw ESyscallError();
}


void TFile::remove(UStr name, bool ignore_if_no_file) {
    const char* name_ptr = name.get_c_str().begin();
    remove(name_ptr, ignore_if_no_file);
}


void TFile::rename(const char* from, const char* to) {
    if (::rename(from, to) == -1)
        throw ESyscallError();
}


void TFile::rename(UStr from, UStr to) {
    const char* from_cstr = from.get_c_str().begin();
    const char* to_cstr = to.get_c_str().begin();
    rename(from_cstr, to_cstr);
}


TRefStr path_join(StrView op1, StrView op2) {
    TRefStr result;
    bool addslash =
        op1.size() != 0 &&
        op2.size() != 0 &&
        op1.back() != FILESYSTEM_PATH_DELIMETER &&
        op2.front() != FILESYSTEM_PATH_DELIMETER;
    result.reserve(op1.size() + op2.size() + addslash);
    result.append(op1.begin(), op1.end());
    if (addslash)
        result.push_back(FILESYSTEM_PATH_DELIMETER);
    result.append(op2.begin(), op2.end());
    return result;
}

off_t TFile::get_size(UStr name) {
    struct stat buf;
    int res = stat(name.get_c_str().begin(), &buf);
    if (res == -1)
        throw ESyscallError();
    return buf.st_size;
}


bool TFile::exists(UStr name) {
    struct stat buf;
    int res = stat(name.get_c_str().begin(), &buf);
    if (res == 0)
        return true;
    if (errno == ENOENT)
        return false;
    throw ESyscallError();
}


} // namespace bsc
