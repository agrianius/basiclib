/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <atomic>
#include "types.hh"

namespace bsc {

class LoopJobLock {
public:
    LoopJobLock() noexcept = default;
    LoopJobLock(LoopJobLock&& move) = default;

    LoopJobLock(const LoopJobLock&) = delete;
    void operator=(const LoopJobLock&) = delete;
    void operator=(LoopJobLock&&) = delete;

    enum i8 {
        NONE = 0,
        START = 1,
        INLOOP = 2,
    };

    // Returns true if job should be started
    // otherwise returns false
    bool Start() noexcept {
        for (;;) {
            i8 current = Lock_.load();
            if (current & START)
                return false;
            if (Lock_.compare_exchange_strong(current, (i8)(current | START)))
                return !(current & INLOOP);
        }
    }

    void Stop() noexcept {
        for (;;) {
            i8 current = Lock_.load();
            if (!(current & START))
                return;
            if (Lock_.compare_exchange_strong(current, (i8)(current & ~START)))
                return;
        }
    }

    inline bool Enter() noexcept {
        i8 current = START;
        return Lock_.compare_exchange_strong(current, (i8)(START | INLOOP));
    }

    // Call Leave only after successive Enter returned true
    // Don't call MayContinue after Leave returned true
    inline bool Leave() noexcept {
        i8 current = INLOOP;
        return Lock_.compare_exchange_strong(current, NONE);
    }

    // Call MayContinue only after Enter returned true
    // Don't call Leave after MayContinue returned false
    bool MayContinue() noexcept {
        for (;;) {
            i8 current = Lock_.load();
            if (current & START)
                return true;
            if (Leave())
                return false;
        }
    }

protected:
    std::atomic<i8> Lock_ = {NONE};
};

} // namespace bsc
