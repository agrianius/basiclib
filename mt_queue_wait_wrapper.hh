/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#pragma once
#include "mt_semaphore.hh"

namespace bsc {


namespace detail {

template <typename PayloadType, typename QueueType>
struct DequeueSelector {
    template <typename...Rest>
    static PayloadType
    dequeue(QueueType& queue, Semaphore& sem, Rest&&...rest) {
        PayloadType result;
        for (;;) {
            if (queue.dequeue(&result))
                return result;
            sem.Wait(std::forward<Rest>(rest)...);
        }
    }

    template <typename...Rest>
    static bool dequeue(
            QueueType& queue,
            Semaphore& sem,
            PayloadType* result,
            Rest&&...rest)
    {
        for (;;) {
            if (queue.dequeue(result))
                return true;
            if (sem.Wait(std::forward<Rest>(rest)...))
                return false;
        }
    }

};

}


template <typename QueueWithin, typename PayloadType>
class Queue_MPSC_WaitWrapper {
public:
    void enqueue(PayloadType msg) {
        Queue.enqueue(std::move(msg));
        if (Sem.Get() <= 0)
            Sem.Post();
    }

    template <typename...Params>
    auto dequeue(Params&&...params) {
        return detail::DequeueSelector<PayloadType, decltype(Queue)>::
            dequeue(Queue, Sem, std::forward<Params>(params)...);
    }

protected:
    QueueWithin Queue;
    Semaphore Sem;
};


} // namespace bsc
