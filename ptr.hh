/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


#pragma once
#include <memory>
#include <atomic>
#include "types.hh"
#include <stdlib.h>
#include <string.h>
#include <type_traits>

namespace ptr {

/* abstract universal holder */
class Holder {
public:
    virtual void Release() = 0;
    virtual ~Holder() = default;
};

template <typename TPayload>
class SharedHolder: public Holder {
public:
    SharedHolder() = default;

    SharedHolder(const std::shared_ptr<TPayload>& ptr)
        : Ptr_(ptr)
    {}

    SharedHolder(std::shared_ptr<TPayload>&& ptr)
        : Ptr_(std::move(ptr))
    {}

    SharedHolder& operator=(const SharedHolder& copy) {
        Ptr_ = copy.Ptr_;
        return *this;
    }

    SharedHolder& operator=(SharedHolder&& copy) {
        Ptr_ = std::move(copy.Ptr_);
        return *this;
    }

    SharedHolder& operator=(const std::shared_ptr<TPayload>& ptr) {
        Ptr_ = ptr;
        return *this;
    }

    SharedHolder& operator=(std::shared_ptr<TPayload>&& ptr) {
        Ptr_ = std::move(ptr);
        return *this;
    }

    void Reset() {
        Ptr_.reset();
    }

    void Release() override {
        Reset();
    }

    std::shared_ptr<TPayload>& Get() noexcept {
        return Ptr_;
    }

    const std::shared_ptr<TPayload>& Get() const noexcept {
        return Ptr_;
    }

    TPayload* operator->() const {
        return Ptr_.get();
    }

protected:
    std::shared_ptr<TPayload> Ptr_;
};


template <template <typename, typename ...> class TContainer,
typename TPayload>
class HoldContainer: public Holder {
public:
    HoldContainer() = default;

    HoldContainer(const TContainer<TPayload>& ptr)
        : Ptr_(ptr)
    {}

    HoldContainer(TContainer<TPayload>&& ptr)
        : Ptr_(std::move(ptr))
    {}

    HoldContainer& operator=(const TContainer<TPayload>& ptr) {
        Ptr_ = ptr;
        return *this;
    }

    HoldContainer& operator=(TContainer<TPayload>&& ptr) {
        Ptr_ = std::move(ptr);
        return *this;
    }

    void Release() override {
        Ptr_.reset();
    }
private:
    TContainer<TPayload> Ptr_;
};


class HoldPtr {
public:
    HoldPtr() = default;

    HoldPtr(Holder* ptr) noexcept: Ptr_(ptr) {}

    HoldPtr(const HoldPtr&) = delete;
    HoldPtr& operator=(const HoldPtr&) = delete;

    HoldPtr(HoldPtr&& move) noexcept {
        Ptr_ = move.Ptr_;
        move.Ptr_ = nullptr;
    }

    HoldPtr& operator=(HoldPtr&& move) {
        if (Ptr_ != nullptr)
            Ptr_->Release();

        Ptr_ = move.Ptr_;
        move.Ptr_ = nullptr;
        return *this;
    }

    void Reset(Holder* ptr = nullptr) {
        if (Ptr_ != nullptr)
            Ptr_->Release();

        Ptr_ = ptr;
    }

    Holder* Release() noexcept {
        auto result = Ptr_;
        Ptr_ = nullptr;
        return result;
    }

    ~HoldPtr() {
        if (Ptr_ != nullptr)
            Ptr_->Release();
    }

protected:
    Holder* Ptr_ = nullptr;
};

class TShrPtr {
public:
    TShrPtr() = default;

    TShrPtr(std::atomic<ui64>* counter,
            void* object,
            void (*destroy)(TShrPtr*) noexcept = &DestroyObject)
        : PCounter_(counter)
        , PObject_(object)
        , DestroyCall_(destroy)
    {
        if (PCounter_ != nullptr)
            ++*PCounter_;
    }

    TShrPtr(const TShrPtr& copy)
        : PCounter_(copy.PCounter_)
        , PObject_(copy.PObject_)
        , DestroyCall_(copy.DestroyCall_)
    {
        if (PCounter_ != nullptr)
            ++*PCounter_;
    }

    TShrPtr(TShrPtr&& move)
        : PCounter_(move.PCounter_)
        , PObject_(move.PObject_)
        , DestroyCall_(move.DestroyCall_)
    {
        move.PCounter_ = nullptr;
        move.PObject_ = nullptr;
        move.DestroyCall_ = nullptr;
    }

    TShrPtr& operator=(const TShrPtr& copy) {
        if (PCounter_ != nullptr)
            if (--*PCounter_ == 0)
                if (DestroyCall_ != nullptr)
                    DestroyCall_(this);

        PCounter_ = copy.PCounter_;
        PObject_ = copy.PObject_;
        DestroyCall_ = copy.DestroyCall_;

        if (PCounter_ != nullptr)
            ++*PCounter_;

        return *this;
    }

    TShrPtr& operator=(TShrPtr&& move) {
        if (PCounter_ != nullptr)
            if (--*PCounter_ == 0)
                if (DestroyCall_ != nullptr)
                    DestroyCall_(this);

        PCounter_ = move.PCounter_;
        PObject_ = move.PObject_;
        DestroyCall_ = move.DestroyCall_;

        move.PCounter_ = nullptr;
        move.PObject_ = nullptr;
        move.DestroyCall_ = nullptr;

        return *this;
    }

    ~TShrPtr() {
        if (PCounter_ != nullptr)
            if (--*PCounter_ == 0)
                if (DestroyCall_ != nullptr)
                    DestroyCall_(this);
    }

protected:
    static void DestroyObject(TShrPtr* ptr) noexcept {
        free(ptr->PObject_);
        delete ptr->PCounter_;
    }

    std::atomic<ui64>* PCounter_ = nullptr;
    void* PObject_ = nullptr;
    void (*DestroyCall_)(TShrPtr*) noexcept = &DestroyObject;

    template <typename, bool>
    friend class TTypedShrPtr;
};


template <typename TObject,
          bool triv_dtor = std::is_trivially_destructible<TObject>::value>
class TTypedShrPtr: public TShrPtr {
public:
    TTypedShrPtr(std::atomic<ui64>* counter, TObject* object)
        : TShrPtr(counter, object, &DestroyObject)
    {}

    TTypedShrPtr() = default;
    TTypedShrPtr(const TTypedShrPtr&) = default;
    TTypedShrPtr(TTypedShrPtr&&) = default;

protected:
    static void DestroyObject(TShrPtr* ptr) noexcept {
        delete static_cast<TObject*>(ptr->PObject_);
        delete ptr->PCounter_;
    }
};


template <typename TObject>
class TTypedShrPtr<TObject, true>: public TShrPtr {
public:
    TTypedShrPtr(std::atomic<ui64>* counter, TObject* object)
        : TShrPtr(counter, object)
    {}

    TTypedShrPtr() = default;
    TTypedShrPtr(const TTypedShrPtr&) = default;
    TTypedShrPtr(TTypedShrPtr&&) = default;
};


} // namespace ptr
