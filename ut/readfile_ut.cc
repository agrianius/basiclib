/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <file/readfile.hh>
#include <str/string.hh>
#include <fstream>

#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using bsc::TBufReadFile;
using bsc::CStr;

TEST_GROUP(GReadFile) {};

constexpr const char* TEST_FILE_NAME = "test-file";

void PrepareFile() {
	std::ofstream ofile(TEST_FILE_NAME, std::ios_base::trunc);
	ofile << "lalalalala\nablabal\nhere we go again";
	ofile.close();
}

void CleanUp() {
	unlink(TEST_FILE_NAME);
}


TEST(GReadFile, OpenAndReadlines) {
	PrepareFile();
	TBufReadFile rfile((CStr(TEST_FILE_NAME)));
	auto line = rfile.readline();
	CHECK_EQUAL(line.size(), 11);
	CHECK(std::equal(line.begin(), line.end(), "lalalalala\n"));
	line = rfile.readline();
	CHECK_EQUAL(line.size(), 8);
	CHECK(std::equal(line.begin(), line.end(), "ablabal\n"));
	line = rfile.readline();
	CHECK_EQUAL(line.size(), 16);
	CHECK(std::equal(line.begin(), line.end(), "here we go again"));
	line = rfile.readline();
	CHECK_EQUAL(line.size(), 0);
	CleanUp();
}
