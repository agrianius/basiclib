/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <file/writefile.hh>
#include <str/string.hh>
#include <fstream>

#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using bsc::TBufWriteFile;
using bsc::CStr;

TEST_GROUP(TBufWriteFile) {};

TEST(TBufWriteFile, Write) {
	{
		TBufWriteFile wfile(CStr("test-write-file"));
		wfile.write("hello mekka!\n", 13);
		wfile.write("you are welcome\n", 16);
		wfile.flush();
	}

	std::ifstream infile("test-write-file", std::ifstream::in);
	char checkbuf[64];
	memset(checkbuf, 0, sizeof(checkbuf));
	infile.read(checkbuf, 29);
	STRCMP_EQUAL("hello mekka!\nyou are welcome\n", checkbuf);
	infile.read(checkbuf, 1);
	CHECK(infile.eof());

	::unlink("test-write-file");
}

TEST(TBufWriteFile, CheckFlushInDestructor) {
	{
		TBufWriteFile wfile(CStr("test-write-file"));
		wfile.write("hello mekka!\n", 13);
		wfile.write("you are welcome\n", 16);
	}

	std::ifstream infile("test-write-file", std::ifstream::in);
	char checkbuf[64];
	memset(checkbuf, 0, sizeof(checkbuf));
	infile.read(checkbuf, 29);
	STRCMP_EQUAL("hello mekka!\nyou are welcome\n", checkbuf);
	infile.read(checkbuf, 1);
	CHECK(infile.eof());

	::unlink("test-write-file");
}
