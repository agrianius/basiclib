/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <str/refstr.hh>

#include <memory>
#include <map>

#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using namespace bsc;

template <typename StrType>
constexpr size_t strsizeof(StrType&& str) {
	return sizeof(str) - 1;
}

TEST_GROUP(TRefBuf) {};


TEST(TRefBuf, CreateAndFill) {
    size_t sizes[] = {
        1,
        128,
        1024 - sizeof(TRefBuf),
        4096 - sizeof(TRefBuf),
        1024 * 1024 - sizeof(TRefBuf)
    };

    for (size_t i: sizes) {
        std::unique_ptr<TRefBuf> buf(TRefBuf::AllocNew(i));
        memset(buf->data(), 0xAF, i);
    }
}


TEST(TRefBuf, Counting) {
    std::unique_ptr<TRefBuf> buf(TRefBuf::AllocNew(0));

    CHECK_EQUAL(buf->add_ref(), 2U);
    CHECK_EQUAL(buf->add_ref(), 3U);
    CHECK_EQUAL(buf->rem_ref(), 2U);
    CHECK_EQUAL(buf->rem_ref(), 1U);
    CHECK_EQUAL(buf->rem_ref(), 0U);

    CHECK_EQUAL(buf->add_ref_nothreads(), 1U);
    CHECK_EQUAL(buf->add_ref(), 2U);
    CHECK_EQUAL(buf->add_ref_nothreads(), 3U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 2U);
    CHECK_EQUAL(buf->rem_ref(), 1U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 0U);

    CHECK_EQUAL(buf->add_ref_nothreads(), 1U);
    CHECK_EQUAL(buf->add_ref_nothreads(), 2U);
    CHECK_EQUAL(buf->add_ref_nothreads(), 3U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 2U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 1U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 0U);
}


TEST(TRefBuf, SetAndGetFilled) {
    std::unique_ptr<TRefBuf> buf(TRefBuf::AllocNew(32));

    CHECK_EQUAL(buf->get_filled(), 0U);
    buf->set_filled(3);
    CHECK_EQUAL(buf->get_filled(), 3U);
    buf->set_filled(0);
    CHECK_EQUAL(buf->get_filled(), 0U);
}


TEST(TRefBuf, ConstCounting) {
    std::unique_ptr<const TRefBuf> buf(TRefBuf::AllocNew(32));

    CHECK_EQUAL(buf->add_ref(), 2U);
    CHECK_EQUAL(buf->add_ref(), 3U);
    CHECK_EQUAL(buf->rem_ref(), 2U);
    CHECK_EQUAL(buf->rem_ref(), 1U);
    CHECK_EQUAL(buf->rem_ref(), 0U);

    CHECK_EQUAL(buf->add_ref_nothreads(), 1U);
    CHECK_EQUAL(buf->add_ref(), 2U);
    CHECK_EQUAL(buf->add_ref_nothreads(), 3U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 2U);
    CHECK_EQUAL(buf->rem_ref(), 1U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 0U);

    CHECK_EQUAL(buf->add_ref_nothreads(), 1U);
    CHECK_EQUAL(buf->add_ref_nothreads(), 2U);
    CHECK_EQUAL(buf->add_ref_nothreads(), 3U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 2U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 1U);
    CHECK_EQUAL(buf->rem_ref_nothreads(), 0U);
}


TEST(TRefBuf, is_alone) {
    std::unique_ptr<const TRefBuf> buf(TRefBuf::AllocNew(32));

    CHECK(buf->is_alone());
    CHECK_EQUAL(buf->add_ref(), 2U);
    CHECK_FALSE(buf->is_alone());
    CHECK_EQUAL(buf->add_ref(), 3U);
    CHECK_FALSE(buf->is_alone());
    CHECK_EQUAL(buf->rem_ref(), 2U);
    CHECK_FALSE(buf->is_alone());
    CHECK_EQUAL(buf->rem_ref(), 1U);
    CHECK(buf->is_alone());
}


TEST(TRefBuf, AllocCopy) {
    std::unique_ptr<TRefBuf> buf(TRefBuf::AllocNew(32));

    constexpr char checkStr[] = "Check that string";
    memcpy(buf->data(), checkStr, sizeof(checkStr));
    buf->set_filled(strlen(checkStr));

    std::unique_ptr<TRefBuf> copy(buf->AllocCopy());
    CHECK_EQUAL(memcmp(buf->data(), copy->data(), sizeof(checkStr)), 0);
}


TEST_GROUP(TRefStr) {};

TEST(TRefStr, CreateEmpty) {
    TRefStr str;
    CHECK(str.empty());
    CHECK_EQUAL(str.length(), 0);
}

TEST(TRefStr, Assign) {
    TRefStr str1;
    constexpr char static_str1[] = "You are welcome";
    str1.assign(static_str1, static_str1 + strlen(static_str1));
    STRCMP_EQUAL(static_str1, str1.c_str());
    CHECK_EQUAL(str1.get_reference_count(), 1);

    TRefStr str2 = str1;
    STRCMP_EQUAL(static_str1, str2.c_str());
    CHECK_EQUAL(str1.get_reference_count(), 2);
    CHECK_EQUAL(str2.get_reference_count(), 2);

    constexpr char static_str2[] = "Thank you";
    str2.assign(static_str2, static_str2 + strlen(static_str2));

    STRCMP_EQUAL(static_str1, str1.c_str());
    STRCMP_EQUAL(static_str2, str2.c_str());
    CHECK_EQUAL(str1.get_reference_count(), 1);
    CHECK_EQUAL(str2.get_reference_count(), 1);
}

TEST(TRefStr, AssignCString) {
    TRefStr str1;

    constexpr const char* static_str1 = "You are welcome";
    str1.assign(static_str1);
    STRCMP_EQUAL(static_str1, str1.c_str());

    constexpr const char* static_str2 = "You are again welcome bro";
    str1.assign(static_str2);
    STRCMP_EQUAL(static_str2, str1.c_str());

    constexpr const char static_str3[] = "Yet another welcome";
    str1.assign(static_str3);
    STRCMP_EQUAL(static_str3, str1.c_str());
}

TEST(TRefStr, OperatorAssignCString) {
    TRefStr str1;

    constexpr const char* static_str1 = "You are welcome";
    str1 = static_str1;
    STRCMP_EQUAL(static_str1, str1.c_str());

    constexpr const char* static_str2 = "You are again welcome bro";
    str1 = static_str2;
    STRCMP_EQUAL(static_str2, str1.c_str());

    constexpr const char static_str3[] = "Yet another welcome";
    str1 = static_str3;
    STRCMP_EQUAL(static_str3, str1.c_str());
}

TEST(TRefStr, Append) {
	TRefStr str;
	constexpr const char static_empty[] = "";
	constexpr const char static_str1[] = "You are welcome";

	str.append(static_empty, static_empty + strsizeof(static_empty));
	CHECK_EQUAL(str.length(), 0);

	str.append(static_str1, static_str1 + strsizeof(static_str1));
	STRCMP_EQUAL(static_str1, str.c_str());
	CHECK_EQUAL(str.length(), strsizeof(static_str1));

	str.append(static_str1, static_str1 + strsizeof(static_str1));
	STRCMP_EQUAL(str.c_str(), "You are welcomeYou are welcome");
	CHECK_EQUAL(str.length(), strsizeof(static_str1) * 2);
}

TEST(TRefStr, Insert) {
	TRefStr str;
	constexpr const char static_empty[] = "";
	constexpr const char static_str1[] = "You are welcome";
	constexpr const char static_str2[] = "You are welcome, Bro;";

	str.insert(str.cend(), static_empty, static_empty + strsizeof(static_empty));
	CHECK_EQUAL(str.length(), 0);

	str.insert(str.cbegin(), static_empty, static_empty + strsizeof(static_empty));
	CHECK_EQUAL(str.length(), 0);

	str.insert(str.cend(), static_str1, static_str1 + strsizeof(static_str1));
	STRCMP_EQUAL(static_str1, str.c_str());
	CHECK_EQUAL(str.length(), strsizeof(static_str1));

	str.insert(str.cend(), static_str1, static_str1 + strsizeof(static_str1));
	STRCMP_EQUAL(str.c_str(), "You are welcomeYou are welcome");
	CHECK_EQUAL(str.length(), strsizeof(static_str1) * 2);

	str.insert(str.cbegin(), static_str2, static_str2 + strsizeof(static_str2));
	STRCMP_EQUAL(str.c_str(),
		"You are welcome, Bro;You are welcomeYou are welcome");
	CHECK_EQUAL(str.length(), strsizeof(static_str1) * 2 + strsizeof(static_str2));

	str.insert(str.cbegin() + strsizeof(static_str1) + strsizeof(static_str2),
			static_str2, static_str2 + strsizeof(static_str2));
	STRCMP_EQUAL(str.c_str(),
		"You are welcome, Bro;You are welcomeYou are welcome, Bro;You are welcome");
	CHECK_EQUAL(str.length(), strsizeof(static_str1) * 2 + strsizeof(static_str2) * 2);
}

TEST(TRefStr, Reserve) {
	TRefStr str;
	str.reserve(100);
	CHECK(str.empty());

	const char* data1 = str.cbegin();

	constexpr const char static_str[] = "You are welcome";
	str.append(static_str, static_str + strsizeof(static_str));

	CHECK_EQUAL(data1, str.cbegin());
	CHECK_EQUAL(str.size(), 15);
	STRCMP_EQUAL(static_str, str.c_str());

	str.append(static_str, static_str + strsizeof(static_str));
	CHECK_EQUAL(data1, str.cbegin());
	CHECK_EQUAL(str.size(), 30);
	STRCMP_EQUAL("You are welcomeYou are welcome", str.c_str());

	str.insert(str.begin() + 4, static_str, static_str + strsizeof(static_str));
	CHECK_EQUAL(data1, str.cbegin());
	CHECK_EQUAL(str.size(), 45);
	STRCMP_EQUAL("You You are welcomeare welcomeYou are welcome", str.c_str());
}


TEST(TRefStr, DeleteViaUStr) {
    UStr ustr;
    {
        TRefStr str;

        constexpr const char static_str[] = "You are welcome";
        str.append(static_str, static_str + strsizeof(static_str));

        ustr = str;
    }

    CHECK(ustr.is_c_str());
    STRCMP_EQUAL("You are welcome", ustr.begin());
}


TEST(TRefStr, Compare) {
    TRefStr str1("love me");
    TRefStr str2("leave me");
    CHECK(str2 < str1);
}


TEST(TRefStr, ConstCompare) {
    TRefStr const str1("love me");
    TRefStr const str2("leave me");
    CHECK(str2 < str1);
}


TEST(TRefStr, MapKey) {
    std::map<TRefStr, int, std::less<>> dict;
    dict.emplace("love me", 1);
    dict.emplace("leave me", 2);

    const TRefStr key1("love me");
    const TRefStr key2("leave me");

    CHECK(dict[key1] == 1);
    CHECK(dict[key2] == 2);

    const std::map<TRefStr, int, std::less<>> cdict(dict);

    const char* cckey1 = "love me";
    const char* cckey2 = "leave me";

    CHECK(dict.find(cckey1)->second == 1);
    CHECK(dict.find(cckey2)->second == 2);
    CHECK(dict.find(key1)->second == 1);
    CHECK(dict.find(key2)->second == 2);
}


TEST(TRefStr, ConverToUStr) {
    {
        TRefStr str1("love me");
        TRefStr str2 = str1;

        UStr u_str = str1.get_uni_str();

        CHECK_EQUAL(3, str1.get_reference_count());
        CHECK_EQUAL(3, str2.get_reference_count());
    }

    {
        TRefStr str1("love me");
        TRefStr str2 = str1;

        UStr u_str = str1.release_to_uni_str();

        CHECK(str1.empty());
        CHECK_EQUAL(2, str2.get_reference_count());
    }
}


TEST_GROUP(TWeakRefStr) {};

TEST(TWeakRefStr, AssignToRefStr) {
	TRefStr hld;
	{
		TRefStr str("You are welcome");
		TWeakRefStr mediator = str;
		hld = mediator;
		CHECK_EQUAL(2, str.get_reference_count());
		CHECK_EQUAL(2, hld.get_reference_count());
		CHECK_EQUAL(hld.cbegin(), str.cbegin());
	}
	CHECK_EQUAL(15, hld.size());
	STRCMP_EQUAL(hld.c_str(), "You are welcome");
	CHECK_EQUAL(1, hld.get_reference_count());
}
