/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <file/dir.hh>
#include <algorithm>

#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using namespace bsc;

TEST_GROUP(TDir) {};

TEST(TDir, GetList) {
	TDir dir("test_dir");

	auto list = dir.GetList(TDir::LIST_FILE);
	CHECK_EQUAL(list.size(), 3);
	std::sort(list.begin(), list.end());
	STRCMP_EQUAL(list[0].c_str(), "entry1");
	STRCMP_EQUAL(list[1].c_str(), "entry2");
	STRCMP_EQUAL(list[2].c_str(), "entry3");

	list = dir.GetList(TDir::LIST_DIR);
	CHECK_EQUAL(list.size(), 3);
	std::sort(list.begin(), list.end());
	STRCMP_EQUAL(list[0].c_str(), ".");
	STRCMP_EQUAL(list[1].c_str(), "..");
	STRCMP_EQUAL(list[2].c_str(), "entry4");

	list = dir.GetList(TDir::LIST_DIR | TDir::LIST_NOSTD_DIRS);
	CHECK_EQUAL(list.size(), 1);
	STRCMP_EQUAL(list[0].c_str(), "entry4");
}
