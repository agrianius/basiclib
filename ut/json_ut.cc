/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "json.hh"
#include <iostream>

#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using bsc::TJson;
using bsc::TRefStr;

TEST_GROUP(JSON) {};

TEST(JSON, Compilable) {
	TJson doc;
	doc["pi"] = 3.141;
	doc["happy"] = true;
	doc["name"] = "I'm Batman";
	doc["nothing"] = nullptr;
	doc["answer"]["everything"] = 42;
	doc["list"] = { 1, 0, 2 };
	doc["object"] = { {"currency", "USD"}, {"value", 42.99} };

	CHECK(doc.is_object());

	CHECK(doc["pi"].is_number_float());
	CHECK_EQUAL(doc["pi"].get<double>(), 3.141);

	CHECK(doc["name"].is_string());
	CHECK(doc["name"].get<TRefStr>() == TRefStr("I'm Batman"));

	CHECK(doc["nothing"].is_null());
}


TEST(JSON, TRefStr_ref_counting) {
	TJson doc;

	TRefStr sample = "I'm Batman";

	doc = sample;
	CHECK(doc.is_string());
	CHECK_EQUAL(sample.get_reference_count(), 2);

	TRefStr sample_back = doc;
	CHECK_EQUAL(sample.get_reference_count(), 3);
}

TEST(JSON, Serialization) {
	TJson doc;
	doc["pi"] = 3.141;
	doc["happy"] = true;
	doc["name"] = "I'm Batman";
	doc["nothing"] = nullptr;
	doc["answer"]["everything"] = 42;
	doc["list"] = { 1, 0, 2 };
	doc["object"] = { {"currency", "USD"}, {"value", 42.99} };

	TRefStr dump = doc.dump();

	const char* const expect_str =
		R"({"answer":{"everything":42},)"
		R"("happy":true,"list":[1,0,2],)"
		R"("name":"I'm Batman","nothing":null,)"
		R"("object":{"currency":"USD","value":42.99},)"
		R"("pi":3.141})";
	STRCMP_EQUAL(expect_str, dump.c_str());
}
