/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ptr.hh>
#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using namespace ptr;

class TSample {
public:
    TSample() {
        Destroyed = false;
    }

    ~TSample() {
        Destroyed = true;
    }

    static bool Destroyed;
};

bool TSample::Destroyed;


TEST_GROUP(TShrPtr) {};

TEST(TShrPtr, CountAndDestroy) {
    {
    TShrPtr abstract;
    std::atomic<ui64>* cnt = new std::atomic<ui64>(0);
    CHECK_EQUAL(cnt->load(), 0UL);

    {

        TTypedShrPtr<TSample> orig(cnt, new TSample);
        CHECK_EQUAL(cnt->load(), 1UL);
        CHECK_EQUAL(TSample::Destroyed, false);

        abstract = orig;
        CHECK_EQUAL(cnt->load(), 2UL);
    }

    CHECK_EQUAL(cnt->load(), 1UL);
    }

    CHECK_EQUAL(TSample::Destroyed, true);
}
