/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <tcp.hh>
#include <iostream>
#include <thread>
#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>


using bsc::TTcpV4Socket;
using bsc::TTcpV6Socket;
using bsc::TTcpSocket;
using bsc::TTcpServer;
using bsc::TSocket;


TEST_GROUP(TCP) {};

TEST(TCP, CreateSocketV4) {
    TTcpV4Socket sock;
    sock.connect("yandex.ru", 80);
    sock.write("GET / HTTP/1.0\r\nHostname: yandex.ru\r\n\r\n", 39);
    sock.flush();
    auto alldata = sock.read_all();
    std::string allstr(alldata.begin(), alldata.end());
    std::cout << allstr;
}

TEST(TCP, CreateSocketAuto) {
    TTcpSocket sock;
    sock.connect("yandex.ru", 80);
    sock.write("GET / HTTP/1.0\r\nHostname: yandex.ru\r\n\r\n", 39);
    sock.flush();
    auto alldata = sock.read_all();
    std::string allstr(alldata.begin(), alldata.end());
    std::cout << allstr;
}

TEST(TCP, Listening) {
    TTcpServer server_sock(0);

    auto connector = [&]() {
        ui16 port = TSocket::get_local_port(server_sock.get_descriptor());
        TTcpSocket sock;
        sock.connect("::1", port);
        sock.write("A", 1);
    };

    std::thread connecting(connector);

    auto conn = server_sock.accept();
    char read_char;
    conn.read(&read_char, 1);
    CHECK(read_char == 'A');

    connecting.join();
}
