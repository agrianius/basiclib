/*
Licensed under the MIT License <http://opensource.org/licenses/MIT>.

Copyright (c) 2018 Vitaliy Manushkin.

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <str/string.hh>
#include <vector>
#include <algorithm>
#include <str/refstr.hh>

#include <CppUTest/MemoryLeakDetectorNewMacros.h>
#include <CppUTest/TestHarness.h>

using bsc::StrView;

template <typename StrType>
constexpr size_t strsizeof(StrType&& str) {
	return sizeof(str) - 1;
}


TEST_GROUP(StrView) {};

TEST(StrView, trim) {
	char static_str[] = "   You are welcome   ";
    StrView buf(static_str, static_str + strsizeof(static_str));
	buf.trim();
	*const_cast<char*>(buf.end()) = 0;
	STRCMP_EQUAL("You are welcome", buf.begin());
}

TEST(StrView, trim_to_empty) {
	char static_str[] = "      ";
    StrView buf(static_str, static_str + strsizeof(static_str));
	buf.trim();
	CHECK_EQUAL(buf.begin(), buf.end());
}

TEST(StrView, search) {
	constexpr char static_str[] = "You are welcome";
    StrView buf(static_str, static_str + strsizeof(static_str));

	auto found = buf.search('Q');
	CHECK_EQUAL(buf.end(), found);

	found = buf.search('Y');
	CHECK_EQUAL('Y', *found);

	found = buf.search('a');
	CHECK_EQUAL('a', *found);

	found = buf.search('e', found);
	CHECK_EQUAL('e', *found);

	found = buf.search('Y', found);
	CHECK_EQUAL(buf.end(), found);
}

TEST(StrView, split_all) {
	constexpr char static_str[] = "You are welcome";
    StrView buf(static_str, static_str + strsizeof(static_str));

	auto fields = buf.split_all<std::vector>(' ');
	CHECK_EQUAL(3, fields.size());
	CHECK(std::equal(fields[0].begin(), fields[0].end(), buf.begin()));
	CHECK(std::equal(fields[1].begin(), fields[1].end(), buf.begin() + 4));
	CHECK(std::equal(fields[2].begin(), fields[2].end(), buf.begin() + 8));
}

TEST(StrView, get_field) {
	constexpr char static_str[] = "You are welcome";
    StrView buf(static_str, static_str + strsizeof(static_str));

	auto field = buf.get_field(' ');
	CHECK(std::equal(field.begin(), field.end(), buf.begin()));

	field = buf.get_field(' ', 1);
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 4));

	field = buf.get_field(' ', 2);
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 8));

	field = buf.get_field(' ', 3);
	CHECK(field.empty());

	field = buf.get_field(' ', 4);
	CHECK(field.empty());
}

TEST(StrView, get_field_reverse) {
	constexpr char static_str1[] = "  You are welcome  ";
    StrView buf(static_str1, static_str1 + strsizeof(static_str1));

	auto field = buf.get_field_reverse(' ');
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 10));

	field = buf.get_field_reverse(' ', 1);
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 6));

	field = buf.get_field_reverse(' ', 2);
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 2));

	field = buf.get_field_reverse(' ', 3);
	CHECK(field.empty());

	field = buf.get_field_reverse(' ', 4);
	CHECK(field.empty());


	constexpr char static_str2[] = "You are welcome";
    buf = StrView(static_str2, static_str2 + strsizeof(static_str2));

	field = buf.get_field_reverse(' ');
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 8));

	field = buf.get_field_reverse(' ', 1);
	CHECK(std::equal(field.begin(), field.end(), buf.begin() + 4));

	field = buf.get_field_reverse(' ', 2);
	CHECK(std::equal(field.begin(), field.end(), buf.begin()));

	field = buf.get_field_reverse(' ', 3);
	CHECK(field.empty());

	field = buf.get_field_reverse(' ', 4);
	CHECK(field.empty());
}


TEST(StrView, OperatorPlus) {
	constexpr char static_str1[] = "  You are welcome  ";
    auto buf1 = StrView(static_str1,
			static_str1 + strsizeof(static_str1));

	constexpr char static_str2[] = "You are welcome";
    auto buf2 = StrView(static_str2,
			static_str2 + strsizeof(static_str2));

	auto arr = buf1 + buf2;
	CHECK_EQUAL(arr.size(), 2);
	CHECK(arr[0] == buf1);
	CHECK(arr[0].begin() == buf1.begin());
	CHECK(arr[0].end() == buf1.end());
	CHECK(arr[1] == buf2);
	CHECK(arr[1].begin() == buf2.begin());
	CHECK(arr[1].end() == buf2.end());

	constexpr char static_str3[] = "You are very welcome";
    auto buf3 = StrView(static_str3,
			static_str3 + strsizeof(static_str3));

	auto bigger = arr + buf3;
	CHECK_EQUAL(bigger.size(), 3);
	CHECK(bigger[0] == buf1);
	CHECK(bigger[0].begin() == buf1.begin());
	CHECK(bigger[0].end() == buf1.end());
	CHECK(bigger[1] == buf2);
	CHECK(bigger[1].begin() == buf2.begin());
	CHECK(bigger[1].end() == buf2.end());
	CHECK(bigger[2] == buf3);
	CHECK(bigger[2].begin() == buf3.begin());
	CHECK(bigger[2].end() == buf3.end());
}

TEST(StrView, OperatorEqual) {
	constexpr char static_str1[] = "You are welcome";
    auto buf1 = StrView(static_str1,
			static_str1 + strsizeof(static_str1));

	CHECK(buf1 == "You are welcome");
	CHECK(!(buf1 != "You are welcome"));

	CHECK(!(buf1 == "You are welcome not"));
	CHECK(buf1 != "You are welcome not");

	CHECK(!(buf1 == "You are welc"));
	CHECK(buf1 != "You are welc");

	CHECK("You are welcome" == buf1);
	CHECK(!("You are welcome" != buf1));

	CHECK(!("You are welcome not" == buf1));
	CHECK("You are welcome not" != buf1);

	CHECK(!("You are welc" == buf1));
	CHECK("You are welc" != buf1);

	constexpr char static_str2[] = "You are welcome";
    auto buf2 = StrView(static_str2,
			static_str2 + strsizeof(static_str2));

	CHECK(buf1 == buf2);
	CHECK(!(buf1 != buf2));

	constexpr char static_str3[] = "You are very welcome";
    auto buf3 = StrView(static_str3,
			static_str3 + strsizeof(static_str3));

	CHECK(!(buf1 == buf3));
	CHECK(buf1 != buf3);
}
